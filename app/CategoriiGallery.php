<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;


class CategoriiGallery extends Model
{
     use Translatable;
    protected $translatable = ['name'];
}
