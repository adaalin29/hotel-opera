<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use App\Room;


class RoomsController extends Controller{

    public function rooms(){
        
        $camere = Room::withTranslations()->get()->translate(\App::getLocale(), 'ro');
    
        return view ('camere',[

            'camere'=>$camere,
        ]);
    }


    public function detaliu($url_slug){
        

        $camera = Room::where('id',$url_slug)->withTranslations()->firstOrFail()->translate(\App::getLocale(), 'ro');
        if($camera)
        $camera->gallery = json_decode($camera->gallery);
        // dd($camera->gallery);
        return view ('camera-detaliu',[

            'camera'=>$camera,
        ]);
    }
}