<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use App\Statice;
use App\Mail\SendMessage;
use App\Mail\SendReservation;
use Validator;
use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;

class ContactController extends Controller{


    public function index(){

        $ContactContent = Statice::where("pag","contact-content")->withTranslations()->first()->translate(\App::getLocale(), 'ro');

        return view ('contact',[

            'ContactContent'=>$ContactContent,
        ]);
    }
    public function send_message(Request $request){
        $contact_email = setting('site.email');
  
        $form_data = $request->only(['name', 'message','termeni','email','phone']);
        $validationRules = [

            'name'    => ['required','min:3'],
            'email'    => ['required','min:3'],
            'phone'    => ['required','min:3'],
            'message'   => ['required','min:10'],           
            'termeni'   => ['required'],
          
        ];
        $validator = Validator::make($form_data, $validationRules);
        if ($validator->fails())
            return ['success' => false, 'error' => $validator->errors()->all()];  
        else{
            // $date_de_salvat = MessageContact::create($request->all()); 
            Mail::to(strip_tags($contact_email))->send(new SendMessage($request->all()));
  
            return ['success' => true,'successMessage'=>  __('site.succes')];
        }      
    }

    public function send_reservation(Request $request){
        $contact_email = setting('site.email');
  
        $form_data = $request->only(['data', 'adulti','copii','cod','nume','telefon','camera']);
        $validationRules = [

            'cod'    => ['required','min:3'],          
            'nume'    => ['required','min:3'],          
            'telefon'    => ['required','min:3'],          
            'data'   => ['required'],
            'adulti'   => ['required'],
            'copii'   => ['required'],
            'camera'   => ['required'],
          
        ];
        $validator = Validator::make($form_data, $validationRules);
        if ($validator->fails())
            return ['success' => false, 'error' => $validator->errors()->all()];  
        else{
            // $date_de_salvat = MessageContact::create($request->all()); 
            Mail::to(strip_tags($contact_email))->send(new SendReservation($request->all()));
  
            return ['success' => true,'successMessage'=>  __('site.contactati')];
        }      
    }
}