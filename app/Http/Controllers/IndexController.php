<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use App\Statice;
use App\SalaGallery;
use App\WeddingGallery;
use App\RestaurantGallery;
use App\Offer;
use App\Room;
use App\Gallery;
use App\CategoriiGallery;

class IndexController extends Controller{

    public function index(){

        $camere = Room::get();

        $indexHeaderImage = Statice::where("pag","index-header-image")->first();
        $indexHeaderVideo = Statice::where("pag","index-header-video")->first();
        $Section1Title = Statice::where("pag","index-section1-title")->withTranslations()->first()->translate(\App::getLocale(), 'ro');
        $Section1Content = Statice::where("pag","index-section1-content")->withTranslations()->first()->translate(\App::getLocale(), 'ro');
        $Section1Picture = Statice::where("pag","index-section1-picture")->first();
        $Section2PictureLeft = Statice::where("pag","index-section2-picture-left")->first();
        $Section2PictureRight = Statice::where("pag","index-section2-picture-right")->first();
        $Section2PictureTitle = Statice::where("pag","index-section2-title")->withTranslations()->first()->translate(\App::getLocale(), 'ro');
        $Section2PictureContent = Statice::where("pag","index-section2-content")->withTranslations()->first()->translate(\App::getLocale(), 'ro');
        $Section3PictureLeft = Statice::where("pag","index-section3-image-left")->first();
        $Section3PictureRight = Statice::where("pag","index-section3-image-right")->first();
        $Section3PictureTitle = Statice::where("pag","index-section3-title")->withTranslations()->first()->translate(\App::getLocale(), 'ro');
        $Section3PictureContent = Statice::where("pag","index-section3-content")->withTranslations()->first()->translate(\App::getLocale(), 'ro');
        $IndexDeliciiTitle = Statice::where("pag","index-delicii-title")->withTranslations()->first()->translate(\App::getLocale(), 'ro');
        $IndexDeliciiSubtitle = Statice::where("pag","index-delicii-subtitle")->withTranslations()->first()->translate(\App::getLocale(), 'ro');
        $IndexLocatieTitle = Statice::where("pag","index-locatie-title")->withTranslations()->first()->translate(\App::getLocale(), 'ro');
        $IndexLocatieContent = Statice::where("pag","index-locatie-content")->withTranslations()->first()->translate(\App::getLocale(), 'ro');

        // if($indexHeader != null && $indexHeader->images != null ){
        //     $indexHeader->images = json_decode($indexHeader->images);
        // }
        return view('index',[
            
            'camere'=>$camere,
            'indexHeaderImage' => $indexHeaderImage,
            'indexHeaderVideo' => $indexHeaderVideo,
            'Section1Title' => $Section1Title,
            'Section1Content' => $Section1Content,
            'Section1Picture' => $Section1Picture,
            'Section2PictureLeft' => $Section2PictureLeft,
            'Section2PictureRight' => $Section2PictureRight,
            'Section2PictureTitle' => $Section2PictureTitle,
            'Section2PictureContent' => $Section2PictureContent,
            'IndexDeliciiTitle' => $IndexDeliciiTitle,
            'IndexDeliciiSubtitle' => $IndexDeliciiSubtitle,
            'IndexLocatieTitle' => $IndexLocatieTitle,
            'IndexLocatieContent' => $IndexLocatieContent,
            'Section3PictureLeft' => $Section3PictureLeft,
            'Section3PictureRight' => $Section3PictureRight,
            'Section3PictureTitle' => $Section3PictureTitle,
            'Section3PictureContent' => $Section3PictureContent,
        ]);
    }

    public function evenimente(){

        $poze_sala = Gallery::where('category_id','1')->get();
        $poze_nunta = WeddingGallery::get();

        foreach($poze_nunta as $gal){
            $gal->image = json_decode($gal->image, true);
          }

        foreach($poze_sala as $gal){
            $gal->images = json_decode($gal->images, true);
          }

        $EvenimentPoza = Statice::where("pag","evenimente-poza")->first();
        $EvenimentDescriere = Statice::where("pag","evenimente-descriere")->withTranslations()->first()->translate(\App::getLocale(), 'ro');
        $EvenimentNuntaPoza = Statice::where("pag","evenimente-nunti-poza")->first();
        $EvenimentNuntaDescriere = Statice::where("pag","evenimente-nunti-descriere")->withTranslations()->first()->translate(\App::getLocale(), 'ro');

        return view('evenimente',[

            'EvenimentPoza'=>$EvenimentPoza,
            'EvenimentDescriere'=>$EvenimentDescriere,
            'EvenimentNuntaPoza'=>$EvenimentNuntaPoza,
            'EvenimentNuntaDescriere'=>$EvenimentNuntaDescriere,
            'poze_sala'=>$poze_sala,
            'poze_nunta'=>$poze_nunta,

        ]);
    }

    public function restaurant(){

        $restaurantGallery = Gallery::where('category_id','2')->get();
        $IndexLocatieTitle = Statice::where("pag","index-delicii-title")->withTranslations()->first()->translate(\App::getLocale(), 'ro');
        $IndexLocatieContent = Statice::where("pag","index-delicii-subtitle")->withTranslations()->first()->translate(\App::getLocale(), 'ro');
        

        foreach($restaurantGallery as $gal){
            $gal->images = json_decode($gal->images, true);
          }
        //   dd($restaurantGallery);

        $RestaurantSection1Title = Statice::where("pag","restaurant-section1-title")->withTranslations()->first()->translate(\App::getLocale(), 'ro');
        $RestaurantSection1Content = Statice::where("pag","restaurant-section1-content")->withTranslations()->first()->translate(\App::getLocale(), 'ro');
        $RestaurantSection1Image = Statice::where("pag","restaurant-section1-image")->first();
        $RestaurantMeniu = Statice::where("pag","restaurant-meniu")->withTranslations()->first()->translate(\App::getLocale(), 'ro');
        $RestaurantSection2Title = Statice::where("pag","restaurant-section2-title")->withTranslations()->first()->translate(\App::getLocale(), 'ro');
        $RestaurantSection2Content = Statice::where("pag","restaurant-section2-content")->withTranslations()->first()->translate(\App::getLocale(), 'ro');
        $RestaurantSection2ImageLeft= Statice::where("pag","restaurant-section2-image-left")->first();
        $RestaurantSection2ImageRight= Statice::where("pag","restaurant-section2-image-right")->first();

        return view('restaurant',[

            'RestaurantSection1Title'=>$RestaurantSection1Title,
            'RestaurantSection1Content'=>$RestaurantSection1Content,
            'RestaurantSection1Image'=>$RestaurantSection1Image,
            'RestaurantMeniu'=>$RestaurantMeniu,
            'RestaurantSection2Title'=>$RestaurantSection2Title,
            'RestaurantSection2Content'=>$RestaurantSection2Content,
            'RestaurantSection2ImageLeft'=>$RestaurantSection2ImageLeft,
            'RestaurantSection2ImageRight'=>$RestaurantSection2ImageRight,
            'IndexLocatieTitle'=>$IndexLocatieTitle,
            'IndexLocatieContent'=>$IndexLocatieContent,

            'restaurantGallery'=>$restaurantGallery,
            


        ]);
    }

    public function galerie(){


        $camere = Room::get();
        $galerie = Gallery::get();
        $category = CategoriiGallery::withTranslations()->get()->translate(\App::getLocale(), 'ro');

        foreach($camere as $camera){
            $camera->gallery = json_decode($camera->gallery);
        }
        foreach($galerie as $galerie_camera){
            $galerie_camera->images = json_decode($galerie_camera->images);
        }
        // if($camere)
        return view ('galerie',[

            'camere'=>$camere,
            'galerie'=>$galerie,
            'category'=>$category,

        ]);
    }

    public function servicii(){

        $ServiciiSection1Title= Statice::where("pag","servicii-section1-title")->withTranslations()->first()->translate(\App::getLocale(), 'ro');
        $ServiciiSection1Content= Statice::where("pag","servicii-section1-content")->withTranslations()->first()->translate(\App::getLocale(), 'ro');
        $ServiciiSection1ImageLeft= Statice::where("pag","servicii-section1-image-left")->first();
        $ServiciiSection1ImageRight= Statice::where("pag","servicii-section1-image-right")->first();
        $ServiciiSection1Content2= Statice::where("pag","servicii-section1-content2")->withTranslations()->first()->translate(\App::getLocale(), 'ro');
        $ServiciiSection1Title2= Statice::where("pag","servicii-section1-title2")->withTranslations()->first()->translate(\App::getLocale(), 'ro');
        $ServiciiBannerContent= Statice::where("pag","servicii-banner-content")->withTranslations()->first()->translate(\App::getLocale(), 'ro');

        $ServiciiSection2Title= Statice::where("pag","servicii-section2-title")->withTranslations()->first()->translate(\App::getLocale(), 'ro');
        $ServiciiSection2Content= Statice::where("pag","servicii-section2-content")->withTranslations()->first()->translate(\App::getLocale(), 'ro');
        $ServiciiSection2ImageLeft= Statice::where("pag","servicii-section2-image-left")->first();
        $ServiciiSection2ImageRight= Statice::where("pag","servicii-section2-image-right")->first();



        return view ('servicii',[

            'ServiciiSection1Title'=>$ServiciiSection1Title,
            'ServiciiSection1Content'=>$ServiciiSection1Content,
            'ServiciiSection1ImageLeft'=>$ServiciiSection1ImageLeft,
            'ServiciiSection1ImageRight'=>$ServiciiSection1ImageRight,
            'ServiciiSection1Content2'=>$ServiciiSection1Content2,
            'ServiciiSection1Title2'=>$ServiciiSection1Title2,
            'ServiciiBannerContent'=>$ServiciiBannerContent,
            'ServiciiSection2Title'=>$ServiciiSection2Title,
            'ServiciiSection2Content'=>$ServiciiSection2Content,
            'ServiciiSection2ImageLeft'=>$ServiciiSection2ImageLeft,
            'ServiciiSection2ImageRight'=>$ServiciiSection2ImageRight,

        ]);
    }

    public function oferte(){

        $oferte = Offer::get();

        $OferteSection1Title1= Statice::where("pag","oferte-section1-title1")->withTranslations()->first()->translate(\App::getLocale(), 'ro');
        $OferteSection1Content1= Statice::where("pag","oferte-section1-content1")->withTranslations()->first()->translate(\App::getLocale(), 'ro');
        $OferteSection1ImageLeft= Statice::where("pag","oferte-section1-image-left")->first();
        $OferteSection1ImageRight= Statice::where("pag","oferte-section1-image-right")->first();
        $OferteSection1Title2= Statice::where("pag","oferte-section1-title2")->withTranslations()->first()->translate(\App::getLocale(), 'ro');
        $Content2= Statice::where("pag","content2")->withTranslations()->first()->translate(\App::getLocale(), 'ro');

        return view('oferte',[

           'OferteSection1Title1'=>$OferteSection1Title1,
           'OferteSection1Content1'=>$OferteSection1Content1,
           'OferteSection1ImageLeft'=>$OferteSection1ImageLeft,
           'OferteSection1ImageRight'=>$OferteSection1ImageRight,
           'OferteSection1Title2'=>$OferteSection1Title2,
           'Content2'=>$Content2,
           'oferte'=>$oferte,
        ]);
    }

    public function oferte_detaliu($url_slug){
        
        $oferta = Offer::where('id',$url_slug)->firstOrFail();
        $alteOferte = Offer::where('id','!=',$oferta->id)->orderBy('created_at','desc')->take(4)->get();
        return view ('oferta-detaliu',[

            'oferta'=>$oferta,
            'alteOferte'=>$alteOferte,
        ]);
    }


    public function politica(){

        $politicaText = Statice::where("pag","politica")->withTranslations()->first()->translate(\App::getLocale(), 'ro');
        return view('politica',[

            'politicaText' =>$politicaText,
        ]);
    }

    public function termeni(){
        
        $termeniText = Statice::where("pag","termeni")->withTranslations()->first()->translate(\App::getLocale(), 'ro');
        return view('termeni',[

            'termeniText' =>$termeniText,
        ]);
    }

    public function cookies(){
        
        $cookiesText = Statice::where("pag","cookies")->withTranslations()->first()->translate(\App::getLocale(), 'ro');
        return view('cookies',[

            'cookiesText' =>$cookiesText,
        ]);
    }
}