<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\News;
use App\Room;
use View;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $rooms = Room::get();
        $news = News::get()->toArray();


        View::share('news',$news);
        View::share('rooms',$rooms);
    }
}
