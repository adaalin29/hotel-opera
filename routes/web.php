<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController@index');
Route::get('evenimente', 'IndexController@evenimente');
Route::get('restaurant', 'IndexController@restaurant');
Route::get('servicii', 'IndexController@servicii');
Route::get('contact', 'ContactController@index');
Route::get('oferte', 'IndexController@oferte');
Route::get('politica', 'IndexController@politica');
Route::get('cookies', 'IndexController@cookies');
Route::get('termeni', 'IndexController@termeni');
Route::get('camere', 'RoomsController@rooms');
Route::get('galerie', 'IndexController@galerie');
Route::get('/camera-detaliu/{url_slug}','RoomsController@detaliu');
Route::get('/oferta-detaliu/{url_slug}','IndexController@oferte_detaliu');

Route::post('send-message','ContactController@send_message');
Route::post('send-reservation','ContactController@send_reservation');

Route::get('/storage/thumb/{query}/{file?}', 'ThumbController@index')
    ->where([
        'query' => '[A-Za-z0-9\:\;\-]+',
        'file'  => '[A-Za-z0-9\/\.\-\_]+',
    ])
    ->name('thumb');

    Route::get('locale/{locale}', function($locale) {
        \Session::put('locale', $locale);
        return redirect()->back();
      });


Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});
