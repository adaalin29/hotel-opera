<?php 

 return [

  //  Header

  'camere'=>'Zimmer',
  'restaurant'=>'Restaurant',
  'oferte'=>'Angebote',
  'evenimente'=>'Geschehen',
  'galerie'=>'Galerie',
  'opera'=>'Opernresidenz',
  'data'=>'Sosire - Abfahrt',
  'data-plecare'=>'Abfahrt',
  'data-sosire'=>'Ankunft',
  'adulti'=>'Erwachsene',
  'copii'=>'Kinder',
  'cod'=>'Werbecode',
  'nume'=>'Vor- und Nachname',
  'numar'=>'Telefonnummer',
  'valabilitate'=>'Siehe Gültigkeit',
  'introdu-date'=>'Geben Sie Ihre Ankunfts- und Abreisedaten ein',
  'camera'=>'Kamera',
  'select-room'=>'Wählen Sie die Kamera aus',
  'contactati'=>'Die Reservierung wurde gesendet, Sie werden von einem unserer Betreiber kontaktiert',

  'oferte-valabile'=>'gültige Angebote',

  'rezerva-online'=>'Online buchen',

  // *******************

  // Footer

  'informatii'=>'Nützliche Informationen',
  'about'=>'Über uns',
  'more'=>'Mehr',
  'politica'=>'Datenschutzerklärung',
  'cookies'=>'Cookie-Richtlinie',
  'terms' =>'Allgemeine Geschäftsbedingungen',
  'copy'=>'© Copyright 2020 - Entwickelt von Touch Media. Alle Rechte vorbehalten.',
  'servicii'=>'Servicii',
  'acasa' =>'Zuhause',
  'informatii-rezervare'=>'Buchungsinformationen',
  'rezerva'=>'Reserve',
  'back'=>'früher',

  // *********************

  // Content

  'element1-text'=>'Aktivitäten',
  'element2-text'=>'Transport und Parken',
  'element3-text'=>'Terrasse und privater Strandbereich',
  'element4-text'=>'Hoteldienstleistungen und -einrichtungen',
  'element5-text'=>'Restaurant, Café und Bar',
  'element6-text'=>'Familienzimmer',
  'element7-text'=>'Geschäftstreffen und Veranstaltungen',
  'element8-text'=>'Sofortiger Zugang zum Strand',
  'element9-text'=>'Schwimmen',
  'camerele-noastre'=>'Unsere Zimmer',

  'index-header-title'=>'Das Hotel bietet ein luxuriöses Erlebnis',
  'index-header-subtitle'=>'Entspannen Sie sich und genießen Sie das Meer! Komfort und Entspannung am Schwarzen Meer.',
  'detalii'=>'Details',

  'afla'=>'Erfahren Sie mehr',
  'vezi-locatia'=>'Sehen Sie den Ort',
  'evenimente'=>'Geschehen',
  'sala'=>'Konferenzraum',
  'contact'=>'Kontaktieren Sie uns',
  'nunti'=>'Organisation von Hochzeiten',
  'facilitati'=>'Hoteleinrichtungen',
  'facilitati-camere'=>'Zimmerausstattung',

  'facilitati1'=>'Aktivitäten',
    'facilitati2'=>'Hoteldienstleistungen und -einrichtungen',
    'facilitati3'=>'Sofortiger Zugang zum Strand',
    'facilitati4'=>'Restaurant, Café und Bar',
    'facilitati5'=>'Transport und Parken',
    'facilitati6'=>'Tagungen und Geschäftsveranstaltungen',
    'facilitati7'=>'Terrasse und privater Strandbereich',

    'facilitati-camere1'=>'Wi-Fi Standard',
    'facilitati-camere2'=>'Extra großes Doppelbett',
    'facilitati-camere3'=>'Installation der Klimaanlage',
    'facilitati-camere4'=>'Minibar und Safe',
    'facilitati-camere5'=>'Kostenlose Privatparkplätze',
    'facilitati-camere6'=>'Familienfreundlich',
    'facilitati-camere7'=>'Badezimmer mit Dusche oder Badewanne',

    'facilitati-button'=>'Alle Einrichtungen anzeigen',

    'meniu-restaurant'=>'Restaurantmenü',
    'oferte-buton'=>'Mehr',
    'restaurant1'=>'Frühstück',
    'restaurant2'=>'Vorspeisen',
    'restaurant3'=>'Suppen und Suppen',
    'restaurant4'=>'Das Hauptgericht',
    'restaurant5'=>'Salate und Toppings',
    'restaurant6'=>'Zusätzliche Produkte',
    'restaurant7'=>'Desserts',
    'restaurant8'=>'Getränk',

    'see-menu'=>'Siehe das Menü',

    'servicii-banner-title'=>'Umgebung und Urlaubsaktivitäten',
    'oferte-title'=>'Alle Angebote',
    'oferte-buton'=>'Siehe Details',
    'oferte-buton-da'=>'Reserve',
  // ***************************

  // Contact
  'trimite'=>'Send',
  'nume-complet'=>'Vollständiger Name',
  'telefon'=>'Telefon',
  'message'=>'Nachricht / Frage',
  'check'=>'Ja, ich bin damit einverstanden, dass meine Daten gespeichert werden, um eine entsprechende Antwort zu erhalten',
  'intra'=>'Geben Sie Google Maps ein',
  'succes'=>'Die Nachricht wurde erfolgreich gesendet',

  'rezerva'=>'Reserve',
];