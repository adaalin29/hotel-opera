<?php 

 return [

  //  Header

  'camere'=>'Camere',
  'restaurant'=>'Restaurant',
  'oferte'=>'Oferte',
  'evenimente'=>'Evenimente',
  'galerie'=>'Galerie',
  'opera'=>'Opera Residence',
  'data'=>'Sosire - Plecare',
  'data-plecare'=>'Plecare',
  'data-sosire'=>'Sosire',
  'adulti'=>'Adulti 18+',
  'copii'=>'Copii 0-18',
  'cod'=>'Cod promotional',
  'nume'=>'Nume si prenume',
  'numar'=>'Numar de telefon',
  'valabilitate'=>'Vezi valabilitatea',
  'introdu-date'=>'Introduceti datele de sosire si de plecare',
  'camera'=>'Camera',
  'select-room'=>'Selecteaza Camera',
  'contactati'=>'Rezervarea a fost trimisa, veti fi contactat de unul dintre operatorii nostri',

  'oferte-valabile'=>'Oferte Valabile',

  'rezerva-online'=>'Rezerva online',

  // *******************

  // Footer
  'oferte-buton'=>'Mai multe',
  'informatii'=>'Informatii utile',
  'about'=>'Despre noi',
  'more'=>'Mai multe',
  'politica'=>'Politica de confidentialitate',
  'cookies'=>'Politica de cookies',
  'terms' =>'Termeni si conditii',
  'copy'=>'© Copyright 2020 - Dezvoltat de Touch Media. Toate drepturile sunt rezervate.',
  'servicii'=>'Servicii',
  'acasa' =>'Acasa',
  'informatii-rezervare'=>'informatii rezervare',
  'rezerva'=>'Rezerva',
  'back'=>'Inapoi',

  // *********************

  // Content

  'element1-text'=>'Activities',
  'element2-text'=>'Transport si parcare',
  'element3-text'=>'Terasa si zona de plaja privata',
  'element4-text'=>'Servicii si facilitati hoteliere',
  'element5-text'=>'Restaurant, cafenea si bar',
  'element6-text'=>'Camere de familie',
  'element7-text'=>'Intalniri si evenimente business',
  'element8-text'=>'Acces imediat la plaja',
  'element9-text'=>'Piscina',
  'camerele-noastre'=>'Camerele noastre',

  'index-header-title'=>'hotel opera experienta de lux',
  'index-header-subtitle'=>'Relaxeaza-te si bucura-te de mare! Confort si relaxare la Marea Neagra.',
  'detalii'=>'Detalii',

  'afla'=>'Afla mai multe',
  'vezi-locatia'=>'Vezi locatia',
  'evenimente'=>'Evenimente',
  'sala'=>'Sala de conferinta',
  'contact'=>'Contacteaza-ne',
  'nunti'=>'Organizare nunti',
  'facilitati'=>'facilitatile hotelului',
  'facilitati-camere'=>'facilitati camere',

  'facilitati1'=>'Activitati',
    'facilitati2'=>'Servicii si facilitati hoteliere',
    'facilitati3'=>'Acces imediat la plaja',
    'facilitati4'=>'Restaurant, cafenea si bar',
    'facilitati5'=>'Transport si parcare',
    'facilitati6'=>'Intaliri si evenimente business',
    'facilitati7'=>'Terasa si zona de plaja privata',

    'facilitati-camere1'=>'Standard Wi-Fi',
    'facilitati-camere2'=>'Pat dublu extra-large',
    'facilitati-camere3'=>'Instalatie climatizare',
    'facilitati-camere4'=>'Minibar si Seif',
    'facilitati-camere5'=>'Parcare privata gratuita',
    'facilitati-camere6'=>'Family friendly',
    'facilitati-camere7'=>'Baie cu dus sau cada',

    'facilitati-button'=>'Vezi toate facilitatile',

    'meniu-restaurant'=>'meniu restaurant',

    'restaurant1'=>'Mic dejun',
    'restaurant2'=>'Aperitive',
    'restaurant3'=>'Ciorbe si supe',
    'restaurant4'=>'Felul principal',
    'restaurant5'=>'Salate si garnituri',
    'restaurant6'=>'Produse extra',
    'restaurant7'=>'Deserturi',
    'restaurant8'=>'Bauturi',

    'see-menu'=>'Vezi meniul',

    'servicii-banner-title'=>'Imprejurimi si activitati de vacanta',
    'oferte-title'=>'Toate ofertele',
    'oferte-buton'=>'Vezi detalii',
    'oferte-buton-da'=>'Rezerva',
  // ***************************

  // Contact
  'trimite'=>'Trimite',
  'nume-complet'=>'Numele complet',
  'telefon'=>'Telefon',
  'message'=>'Mesaj/Intrebare',
  'check'=>'Da, sunt de acord ca datele mele sa fie salvate pentru a primi raspuns, conform',
  'intra'=>'Intra pe google maps',
  'succes'=>'Mesajul a fost trimis cu succes',

  'rezerva'=>'Rezerva',
];