<?php 

 return [

  //  Header

  'camere'=>'Rooms',
  'restaurant'=>'Restaurant',
  'oferte'=>'Deals',
  'evenimente'=>'Events',
  'galerie'=>'Gallery',
  'opera'=>'Opera Residence',
  'data'=>'Arrival - Departure',
  'data-plecare'=>'Check out',
  'data-sosire'=>'Check in',
  'adulti'=>'Adults 18+',
  'copii'=>'Kids 0-18',
  'cod'=>'Promotional code',
  'nume'=>'Name and surname',
  'numar'=>'Phone number',
  'valabilitate'=>'Check availability',
  'introdu-date'=>'Please insert check in and check out data',
  'camera'=>'Room',
  'select-room'=>'Select Room',
  'contactati'=>'The reservation was sent, you will be contacted soon by our operators',

  'oferte-valabile'=>'Valid Offers',

  'rezerva-online'=>'Make reservation',
  

  // *******************
  // Footer

  'informatii'=>'Useful informations',
  'about'=>'About us',
  'more'=>'More',
  'politica'=>'Privacy policy',
  'cookies'=>'Cookies policy',
  'terms' =>'Terms and conditions',
  'copy'=>'© Copyright 2020 - Developed by Touch Media. All Rights Reserved.',
  'servicii'=>'Services',
  'acasa' =>'Home',
  'informatii-rezervare'=>'Reservation informations',
  'rezerva'=>'Book',
  'back'=>'Back',

  // *********************

  // Content

    'element1-text'=>'Activities',
    'element2-text'=>'Transport and parking',
    'element3-text'=>'Terrace and private beach area',
    'element4-text'=>'Hotel services and facilities',
    'element5-text'=>'Restaurant, cafe and bar',
    'element6-text'=>'Family rooms',
    'element7-text'=>'Business meetings and events',
    'element8-text'=>'Immediate access to the beach',
    'element9-text'=>'Pool',
    'camerele-noastre'=>'Our rooms',

    'index-header-title'=>'opera hotel luxury experience',
    'index-header-subtitle'=>'Relax and enjoy the sea! Comfort and relaxation at the Black Sea.',
    'detalii'=>'Details',

    'afla'=>'Find out more',
    'vezi-locatia'=>'See location',
    'evenimente'=>'Events',
    'sala'=>'Conference room',
    'contact'=>'Contact us',
    'nunti'=>'Wedding organization',
    'facilitati'=>'hotel facilities',
    'facilitati-camere'=>'rooms facilities',

    'facilitati1'=>'Activities',
    'facilitati2'=>'Hotel services and facilities',
    'facilitati3'=>'Immediate access to the beach',
    'facilitati4'=>'Restaurant, cafe and bar',
    'facilitati5'=>'Transport and parking',
    'facilitati6'=>'Meetings and business events',
    'facilitati7'=>'Terrace and private beach area',

    'facilitati-camere1'=>'Standard Wi-Fi',
    'facilitati-camere2'=>'Extra-large double bed',
    'facilitati-camere3'=>'Air conditioning',
    'facilitati-camere4'=>'Minibar and safe box',
    'facilitati-camere5'=>'Free private parking',
    'facilitati-camere6'=>'Family friendly',
    'facilitati-camere7'=>'Bathroom with shower or bathtub',

  'facilitati-button'=>'See all facilities',

  'meniu-restaurant'=>'Restaurant Menu',

    'restaurant1'=>'Breakfast',
    'restaurant2'=>'Appetizers',
    'restaurant3'=>'Soups',
    'restaurant4'=>'Felul principal',
    'restaurant5'=>'Salads and toppings',
    'restaurant6'=>'Extra products',
    'restaurant7'=>'Desserts',
    'restaurant8'=>'Beverage',
    
    'see-menu'=>'See menu',

    'servicii-banner-title'=>'Surroundings and holiday activities',
    'oferte-title'=>'All offers',
    'oferte-buton'=>'See details',
    'oferte-buton-da'=>'Make a reservation',
    'rezerva'=>'Book it',

  // ***************************
  'oferte-buton'=>'More details',


  // Contact

  'trimite'=>'Send',
  'nume-complet'=>'Full name',
  'telefon'=>'Phone',
  'message'=>'Message/Question',
  'check'=>'Yes, I agree that my data will be saved in order to receive an answer, according to the ',
  'intra'=>'View on google maps',
  'succes'=>'Message was send',
];