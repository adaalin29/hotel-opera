@extends('parts.template') @section('content')
<div class = "container camere-container">
    <a class = "pagini-link" href = "/" style = "display:block;"><div class = "pagini">{{ __('site.acasa') }} | {{ __('site.restaurant') }}</div></a>
    <div class  = "evenimente-title mobile-hidden">{{ __('site.meniu-restaurant') }}</div>
    <div class = "facilitati-container">
        <div class = "facilitati-element restaurant-element">
            <div class = "facilitati-poza"><img src = "images/restaurant1.svg" class = "full-width" style = "object-fit:fill!important;"></div>
            <div class = "facilitati-text">{{ __('site.restaurant1') }}</div>
        </div>
        <div class = "facilitati-element restaurant-element">
            <div class = "facilitati-poza"><img src = "images/restaurant2.svg" class = "full-width" style = "object-fit:fill!important;"></div>
            <div class = "facilitati-text">{{ __('site.restaurant2') }}</div>
        </div>
        <div class = "facilitati-element restaurant-element">
            <div class = "facilitati-poza"><img src = "images/restaurant3.svg" class = "full-width" style = "object-fit:fill!important;"></div>
            <div class = "facilitati-text">{{ __('site.restaurant3') }}</div>
        </div>
        <div class = "facilitati-element restaurant-element">
            <div class = "facilitati-poza"><img src = "images/restaurant4.svg" class = "full-width" style = "object-fit:fill!important;"></div>
            <div class = "facilitati-text">{{ __('site.restaurant4') }}</div>
        </div>
        <div class = "facilitati-element restaurant-element">
            <div class = "facilitati-poza"><img src = "images/restaurant5.svg" class = "full-width" style = "object-fit:fill!important;"></div>
            <div class = "facilitati-text">{{ __('site.restaurant5') }}</div>
        </div>
        <div class = "facilitati-element restaurant-element">
            <div class = "facilitati-poza"><img src = "images/restaurant6.svg" class = "full-width" style = "object-fit:fill!important;"></div>
            <div class = "facilitati-text">{{ __('site.restaurant6') }}</div>
        </div>
        <div class = "facilitati-element restaurant-element">
            <div class = "facilitati-poza"><img src = "images/restaurant7.svg" class = "full-width" style = "object-fit:fill!important;"></div>
            <div class = "facilitati-text">{{ __('site.restaurant7') }}</div>
        </div>
        <div class = "facilitati-element restaurant-element">
            <div class = "facilitati-poza"><img src = "images/restaurant8.svg" class = "full-width" style = "object-fit:fill!important;"></div>
            <div class = "facilitati-text">{{ __('site.restaurant8') }}</div>
        </div>
    </div>
    <div class = "facilitati-camere-mobile">
        <div class  = "evenimente-title">{{ __('site.meniu-restaurant') }}</div>
        <div class="swiper-container">
            <div class="swiper-wrapper">

              <div class="swiper-slide swiper-slide-normal">
                <div class = "facilitati-element">
                    <div class = "facilitati-poza"><img src = "images/restaurant1.svg" class = "full-width-no-object"></div>
                    <div class = "facilitati-text facilitati-text-camere">{{ __('site.restaurant1') }}</div>
                </div>
              </div>
              <div class="swiper-slide swiper-slide-normal">
                <div class = "facilitati-element">
                    <div class = "facilitati-poza"><img src = "images/restaurant2.svg" class = "full-width-no-object"></div>
                    <div class = "facilitati-text facilitati-text-camere">{{ __('site.restaurant2') }}</div>
                </div>
              </div>
              <div class="swiper-slide swiper-slide-normal">
                <div class = "facilitati-element">
                    <div class = "facilitati-poza"><img src = "images/restaurant3.svg" class = "full-width-no-object"></div>
                    <div class = "facilitati-text facilitati-text-camere">{{ __('site.restaurant3') }}</div>
                </div>
              </div>
              <div class="swiper-slide swiper-slide-normal">
                <div class = "facilitati-element">
                    <div class = "facilitati-poza"><img src = "images/restaurant4.svg" class = "full-width-no-object"></div>
                    <div class = "facilitati-text facilitati-text-camere">{{ __('site.restaurant4') }}</div>
                </div>
              </div>
              <div class="swiper-slide swiper-slide-normal">
                <div class = "facilitati-element">
                    <div class = "facilitati-poza"><img src = "images/restaurant5.svg" class = "full-width-no-object"></div>
                    <div class = "facilitati-text facilitati-text-camere">{{ __('site.restaurant5') }}</div>
                </div>
              </div>
              <div class="swiper-slide swiper-slide-normal">
                <div class = "facilitati-element">
                    <div class = "facilitati-poza"><img src = "images/restaurant6.svg" class = "full-width-no-object"></div>
                    <div class = "facilitati-text facilitati-text-camere">{{ __('site.restaurant6') }}</div>
                </div>
              </div>
              <div class="swiper-slide swiper-slide-normal">
                <div class = "facilitati-element">
                    <div class = "facilitati-poza"><img src = "images/restaurant7.svg" class = "full-width-no-object"></div>
                    <div class = "facilitati-text facilitati-text-camere">{{ __('site.restaurant7') }}</div>
                </div>
              </div>
              <div class="swiper-slide swiper-slide-normal">
                <div class = "facilitati-element">
                    <div class = "facilitati-poza"><img src = "images/restaurant8.svg" class = "full-width-no-object"></div>
                    <div class = "facilitati-text facilitati-text-camere">{{ __('site.restaurant8') }}</div>
                </div>
              </div>
            </div>
            <!-- Add Arrows -->
            <div class="swiper-button-next"></div>
            <div class="swiper-button-prev"></div>
          </div>
    </div>
    <div class="about-index restaurant-index">
        <div class="about-section1 restaurant-no-margin-bottom" data-aos="fade-right">
            <div class="about-section1-left restaurant-left">
            <div class="about-seciton1-title">{!!$RestaurantSection1Title->content!!}</div>
                <div class="about-seciton1-subtitle">{!!$RestaurantSection1Content->content!!}</div>
                <a href="{!!strip_tags($RestaurantMeniu->content)!!}" target = "_blank" style="display:block;"><div class="about-seciton1-button">{{ __('site.see-menu') }}</div></a>
            </div>
            <div class="about-section1-right">
                <div class="empty-div"></div>
                <div class="section1-image"><img class="full-width" src="{{ route('thumb', ['width:800', $RestaurantSection1Image->images]) }}"></div>
            </div>
        </div>
    </div>
    <div class="about-seciton2" data-aos="fade-left">
        <div class="section2-left">
            <div class="section2-top">
                <img src="{{ route('thumb', ['width:800', $RestaurantSection2ImageLeft->images]) }}" class="full-width">
            </div>
            <div class="section2-bottom margin-section2-bottom">
                <div class="section2-title">{!!$RestaurantSection2Title->content!!}</div>
                <div class="section2-content">{!!$RestaurantSection2Content->content!!}</div>
            </div>
        </div>
        <div class="section2-right">
            <img src="{{ route('thumb', ['width:800', $RestaurantSection2ImageRight->images]) }}" class="full-width">
        </div>
    </div>
</div>
<div class = "delicii" style = "background-image:url('../images/locatie.png');">
    <div class=  "delicii-overlay">
        <div class=  "delicii-container">
            <div class = "delicii-title">{!!$IndexLocatieTitle->content!!}</div>
                <div class = "delicii-subtitle">{!!$IndexLocatieContent->content!!}</div>
        <a class = "delicii-link" href = "contact"><div class = "delicii-buton">{{ __('site.contact') }}</div></a>
            </div>
    </div>
</div>
@foreach($restaurantGallery as $galerie)
@if($galerie['images']!=NULL)
<div class = "camere-swiper">
    <div class = "camere-swiper-container">
        <div class="swiper-container">
            <div class="swiper-wrapper">
                @foreach($galerie['images'] as $item)
                <div class="swiper-slide">
                    <div class = "index-camere">
                        <a class="fancybox-width" data-fancybox="gallery" href="{{ route('thumb', ['width:1000', $item]) }}">
                            <img class = "full-width" src = "{{ route('thumb', ['width:1000', $item]) }}">
                        </a>
                    </div>
                </div>
                @endforeach
            </div>
            <!-- Add Pagination -->
            <div class="swiper-pagination"></div>
            <!-- Add Arrows -->
            <div class="swiper-button-next"></div>
            <div class="swiper-button-prev"></div>
            </div>
    </div>
</div>
@endif
@endforeach
@endsection
@push('scripts')
<script>
    var slidesMobileElemente=3;
            if(screen.width<=768) {
                slidesMobileElemente=1;
            }
    $(document).ready(function(){
            var swiper = new Swiper('.camere-swiper-container .swiper-container', {
            centeredSlides: true,
            slidesPerView: slidesMobileElemente,
            spaceBetween: 200,
            slidesPerGroup: 1,
            loop: true,
            loopFillGroupWithBlank: true,
                navigation: {
                    nextEl: '.swiper-button-next',
                    prevEl: '.swiper-button-prev',
                },
                
        });
    });
</script>
@endpush