@extends('parts.template')
@section('content')
    <div class = "index-header">
        {{--<video id="vid" controls autoplay  preload = "auto">
            <source src="{!!strip_tags($indexHeaderVideo->content)!!}" type="video/mp4">--}}
        @include('parts.video_iframe')
        {{-- <img class = "index-header-image" src = "{{ route('thumb', ['width:1920', $indexHeaderImage->images]) }}"> --}}
    </div>
<div class = "container">
    <div class = "index-elemente-contianer">
        <a href = "servicii" class = "index-element-servicii-link">
            <div class = "index-element-servicii">
                <div class = "overlay-animatie"></div>
                <div class = "poza-activitate"><img src = "images/element1.svg" class = "full-width-no-object"></div>
                <div class = "poza-activitate-white"><img src = "images/element1-white.svg" class = "full-width-no-object"></div>
                <div class = "element-text">{{ __('site.element1-text') }}</div>
                <div class = "element-text-white">{{ __('site.element1-text') }}</div>
                <div class = "poza-activitate-sageata"><img src = "images/next.svg" class = "full-width"></div>
                <div class = "poza-activitate-sageata-white"><img src = "images/next-white.svg" class = "full-width"></div>
            </div>
        </a>
        <a href = "servicii" class = "index-element-servicii-link">
            <div class = "index-element-servicii">
                <div class = "overlay-animatie"></div>
                <div class = "poza-activitate"><img src = "images/element2.svg" class = "full-width-no-object"></div>
                <div class = "poza-activitate-white"><img src = "images/element2-white.svg" class = "full-width-no-object"></div>
                <div class = "element-text">{{ __('site.element2-text') }}</div>
                <div class = "element-text-white">{{ __('site.element2-text') }}</div>
                <div class = "poza-activitate-sageata"><img src = "images/next.svg" class = "full-width"></div>
                <div class = "poza-activitate-sageata-white"><img src = "images/next-white.svg" class = "full-width"></div>
            </div>
        </a>
        <a href = "servicii" class = "index-element-servicii-link">
            <div class = "index-element-servicii">
                <div class = "overlay-animatie"></div>
                <div class = "poza-activitate"><img src = "images/element3.svg" class = "full-width-no-object"></div>
                <div class = "poza-activitate-white"><img src = "images/element3-white.svg" class = "full-width-no-object"></div>
                <div class = "element-text">{{ __('site.element3-text') }}</div>
                <div class = "element-text-white">{{ __('site.element3-text') }}</div>
                <div class = "poza-activitate-sageata"><img src = "images/next.svg" class = "full-width"></div>
                <div class = "poza-activitate-sageata-white"><img src = "images/next-white.svg" class = "full-width"></div>
            </div>
        </a>
        <a href = "servicii" class = "index-element-servicii-link">
            <div class = "index-element-servicii">
                <div class = "overlay-animatie"></div>
                <div class = "poza-activitate"><img src = "images/element4.svg" class = "full-width-no-object"></div>
                <div class = "poza-activitate-white"><img src = "images/element4-white.svg" class = "full-width-no-object"></div>
                <div class = "element-text">{{ __('site.element4-text') }}</div>
                <div class = "element-text-white">{{ __('site.element4-text') }}</div>
                <div class = "poza-activitate-sageata"><img src = "images/next.svg" class = "full-width"></div>
                <div class = "poza-activitate-sageata-white"><img src = "images/next-white.svg" class = "full-width"></div>
            </div>
        </a>
        <a href = "servicii" class = "index-element-servicii-link">
            <div class = "index-element-servicii">
                <div class = "overlay-animatie"></div>
                <div class = "poza-activitate"><img src = "images/element5.svg" class = "full-width-no-object"></div>
                <div class = "poza-activitate-white"><img src = "images/element5-white.svg" class = "full-width-no-object"></div>
                <div class = "element-text">{{ __('site.element5-text') }}</div>
                <div class = "element-text-white">{{ __('site.element5-text') }}</div>
                <div class = "poza-activitate-sageata"><img src = "images/next.svg" class = "full-width"></div>
                <div class = "poza-activitate-sageata-white"><img src = "images/next-white.svg" class = "full-width"></div>
            </div>
        </a>
        <a href = "servicii" class = "index-element-servicii-link">
            <div class = "index-element-servicii">
                <div class = "overlay-animatie"></div>
                <div class = "poza-activitate"><img src = "images/element6.svg" class = "full-width-no-object"></div>
                <div class = "poza-activitate-white"><img src = "images/element6-white.svg" class = "full-width-no-object"></div>
                <div class = "element-text">{{ __('site.element6-text') }}</div>
                <div class = "element-text-white">{{ __('site.element6-text') }}</div>
                <div class = "poza-activitate-sageata"><img src = "images/next.svg" class = "full-width"></div>
                <div class = "poza-activitate-sageata-white"><img src = "images/next-white.svg" class = "full-width"></div>
            </div>
        </a>
        <a href = "servicii" class = "index-element-servicii-link">
            <div class = "index-element-servicii">
                <div class = "overlay-animatie"></div>
                <div class = "poza-activitate"><img src = "images/element7.svg" class = "full-width-no-object"></div>
                <div class = "poza-activitate-white"><img src = "images/element7-white.svg" class = "full-width-no-object"></div>
                <div class = "element-text">{{ __('site.element7-text') }}</div>
                <div class = "element-text-white">{{ __('site.element7-text') }}</div>
                <div class = "poza-activitate-sageata"><img src = "images/next.svg" class = "full-width"></div>
                <div class = "poza-activitate-sageata-white"><img src = "images/next-white.svg" class = "full-width"></div>
            </div>
        </a>
        <a href = "servicii" class = "index-element-servicii-link">
            <div class = "index-element-servicii">
                <div class = "overlay-animatie"></div>
                <div class = "poza-activitate"><img src = "images/element8.svg" class = "full-width-no-object"></div>
                <div class = "poza-activitate-white"><img src = "images/element8-white.svg" class = "full-width-no-object"></div>
                <div class = "element-text">{{ __('site.element8-text') }}</div>
                <div class = "element-text-white">{{ __('site.element8-text') }}</div>
                <div class = "poza-activitate-sageata"><img src = "images/next.svg" class = "full-width"></div>
                <div class = "poza-activitate-sageata-white"><img src = "images/next-white.svg" class = "full-width"></div>
            </div>
        </a>
        <a href = "servicii" class = "index-element-servicii-link">
            <div class = "index-element-servicii">
                <div class = "overlay-animatie"></div>
                <div class = "poza-activitate"><img src = "images/element9.svg" class = "full-width-no-object"></div>
                <div class = "poza-activitate-white"><img src = "images/element9-white.svg" class = "full-width-no-object"></div>
                <div class = "element-text">{{ __('site.element9-text') }}</div>
                <div class = "element-text-white">{{ __('site.element9-text') }}</div>
                <div class = "poza-activitate-sageata"><img src = "images/next.svg" class = "full-width"></div>
                <div class = "poza-activitate-sageata-white"><img src = "images/next-white.svg" class = "full-width"></div>
            </div>
        </a>
    </div>
</div>
@if($camere!=null)
<div class = "camere-swiper">
    <div class = "center-title">{{ __('site.camerele-noastre') }}</div>
    <div class = "camere-swiper-container">
        <div class="swiper-container">
            <div class="swiper-wrapper">
                @foreach($camere as $item)

                {{-- sunt pe desktop --}}
                <div class="swiper-slide">
                        <div class = "index-camere">
                            <img class = "full-width" src = "{{ route('thumb', ['width:1000', $item->image]) }}">
                            <div class = "index-camere-overlay"></div>
                            <div class= "swiper-index-camera-text">
                            <div class = "swiper-index-camera-titlu">{{$item->name}}</div>
                                <div class = "swiper-index-camera-subtitlu">{!!\Illuminate\Support\Str::limit($item->description,300,$end = '...')!!}</div>
                                <a href = "camera-detaliu/{{$item->slug}}" style = "display:block;">
                                    <div class = "swiper-index-camera-link">
                                        <div class = "swiper-camera-text">{{ __('site.detalii') }}</div>
                                        <div class = "swiper-camera-linie"></div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                @endforeach
            </div>
            <!-- Add Pagination -->
            <div class="swiper-pagination desktop-hidden"></div>
            <!-- Add Arrows -->
            <div class="swiper-button-next mobile-hidden-modificat"></div>
            <div class="swiper-button-prev mobile-hidden-modificat"></div>
            </div>
    </div>
</div>
@endif

<div class = "container">
    <div class = "about-index no-margin-da">
        <div class = "about-section1 no-margin-da" data-aos="fade-right">
            <div class= "about-section1-left index-right">
                <div class= "about-seciton1-title">{!!strip_tags($Section1Title->content)!!}</div>
                <div class= "about-seciton1-subtitle">{!!strip_tags($Section1Content->content)!!}</div>
                <a href = "oferte" style = "display:block;"><div class= "about-seciton1-button">{{ __('site.detalii') }}</div></a>
            </div>
            <div class= "about-section1-right">
                <div class = "empty-div"></div>
                <div class = "section1-image"><img class = "full-width" src = "{{ route('thumb', ['width:800', $Section1Picture->images]) }}"></div>
            </div>
        </div>
    </div>
    <div class = "about-seciton2" data-aos="fade-left">
        <div class=  "section2-left">
            <div class = "section2-top">
                <img src = "{{ route('thumb', ['width:800', $Section2PictureLeft->images]) }}" class = "full-width">
            </div>
            <div class = "section2-bottom margin-section2-bottom punem-margin">
                <div class = "section2-title">{!!$Section2PictureTitle->content!!}</div>
                <div class = "section2-content">{!!$Section2PictureContent->content!!}</div>
            </div>
        </div>
        <div class = "section2-right">
            <img src = "{{ route('thumb', ['width:800', $Section2PictureRight->images]) }}" class = "full-width">
        </div>
    </div>
</div>
<div class = "delicii">
    <div class=  "delicii-overlay">
        <div class=  "delicii-container">
            <div class = "delicii-title">{!!$IndexDeliciiTitle->content!!}</div>
                <div class = "delicii-subtitle">{!!$IndexDeliciiSubtitle->content!!}</div>
                <a class = "delicii-link" href = "restaurant"><div class = "delicii-buton">{{ __('site.afla') }}</div></a>
            </div>
    </div>
</div>
<div class = "container">
    <div class = "about-seciton2-reversed" style = "margin-top:40px;" data-aos="fade-right">
        <div class=  "section2-left">
            <div class = "section2-top">
                <img src = "{{ route('thumb', ['width:800', $Section3PictureRight->images]) }}" class = "full-width">
            </div>
            <div class = "section2-bottom margin-section2-bottom punem-margin">
                <div class = "section2-title">{!!$Section3PictureTitle->content!!}</div>
                <div class = "section2-content">{!!$Section3PictureContent->content!!}</div>
            </div>
        </div>
        <div class = "section2-right">
            <img src = "{{ route('thumb', ['width:800', $Section3PictureLeft->images]) }}" class = "full-width">
        </div>
    </div>
</div>
<div class = "delicii" style = "background-image:url('../images/locatie.png');">
    <div class=  "delicii-overlay">
        <div class=  "delicii-container">
            <div class = "delicii-title">{!!$IndexLocatieTitle->content!!}</div>
                <div class = "delicii-subtitle">{!!$IndexLocatieContent->content!!}</div>
        <a class = "delicii-link" target = "_blank" href = "http://www.google.com/maps/place/{{setting('site.latitudine')}},{{setting('site.longitudine')}}"><div class = "delicii-buton">{{ __('site.vezi-locatia') }}</div></a>
            </div>
    </div>
</div>
@endsection 
@push('scripts')
<script>
    $(document).ready(function(){


        var slidesMobileElemente=3;
            if(screen.width<=768) {
                slidesMobileElemente=1;
            }
        $('.pop-up-overlay').css('right','0%');
            var swiper = new Swiper('.camere-swiper-container .swiper-container', {
            centeredSlides: true,
            slidesPerView: slidesMobileElemente,
            spaceBetween: 200,
            slidesPerGroup: 1,
            loop: true,
            loopFillGroupWithBlank: true,
                navigation: {
                    nextEl: '.swiper-button-next',
                    prevEl: '.swiper-button-prev',
                },
                pagination: {
                el: '.swiper-pagination',
                clickable: true,
                },
                
        });
    });
</script>
@endpush