@extends('parts.template') @section('content')
<div class = "container camere-container">
    <a class = "pagini-link-lung" href = "camere" style = "display:block;"><div class = "pagini">{{ __('site.acasa') }} | {{ __('site.galerie') }}</div></a>
    <div class = "evenimente-title">{{__('site.galerie')}}</div>
    <div class = "swiper-cateogrii">
        <div class="swiper-container">
            <div class="swiper-wrapper">
                @foreach($camere as $item)
                <div class="swiper-slide swiper-slide-normal">
                    <div class = "categorii-element" category_id = "{{$item['id']}}" >{{$item['name']}}</div>
                </div>
                @endforeach
                @foreach($category as $item)
                <div class="swiper-slide swiper-slide-normal">
                    <div class = "categorii-element" galerie_id = "{{$item['id']}}" >{{$item['name']}}</div>
                </div>
                @endforeach
            </div>
            <!-- Add Arrows -->
            <div class="swiper-pagination"></div>
          </div>
    </div>
    {{-- <div class = "categorii-container">

        @foreach($camere as $item)
        <div class = "categorii-element categorii-element-selected" category_id = "{{$item['id']}}" >{{$item['name']}}</div>
        @endforeach
        @foreach($category as $item)
        <div class = "categorii-element categorii-element-selected" galerie_id = "{{$item['id']}}" >{{$item['name']}}</div>
        @endforeach
    </div> --}}
    <div class = "galerie-container">
        @foreach($camere as $camera)
            @foreach($camera['gallery'] as $poza)
            <div class = "poza-galerie" item_category={{$camera->id}}>
                <a class="fancybox-width" data-fancybox="gallery" href="{{ route('thumb', ['width:1000', $poza]) }}">
                    <img class = "full-width" src = "{{ route('thumb', ['width:1000', $poza]) }}">
                </a>
            </div>
            @endforeach
        @endforeach
        @foreach($galerie as $gal)
            @foreach($gal['images'] as $poza)
            <div class = "poza-galerie" poze_category={{$gal->id}}>
                <a class="fancybox-width" data-fancybox="gallery" href="{{ route('thumb', ['width:1000', $poza]) }}">
                    <img class = "full-width" src = "{{ route('thumb', ['width:1000', $poza]) }}">
                </a>
            </div>
            @endforeach
        @endforeach
        <i aria-hidden="true" class="hidden-elements"></i>
            <i aria-hidden="true" class="hidden-elements"></i>
            <i aria-hidden="true" class="hidden-elements"></i>

    </div>
</div>
@endsection
@push('scripts')
<script>
        $(document).ready(function () {


            var slidesMobileElemente=4;
            if(screen.width<=1024) {
                slidesMobileElemente=2;
            }

            if(screen.width<=768) {
                slidesMobileElemente=1;
            }

            var swiperCategorii = new Swiper('.swiper-cateogrii .swiper-container', {
            slidesPerView: slidesMobileElemente,
            spaceBetween: 30,
            loop: true,
            pagination: {
                el: '.swiper-pagination',
                clickable: true,
            },
            autoplay: {
                delay: 2500,
            },
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },
            });


        $('.categorii-element').click(function () {
            $('.categorii-element').removeClass('categorii-element-selected');
            $(this).toggleClass('categorii-element-selected');
            var categorie_curenta = $(this).attr('category_id');
            var categorie_curenta_galerie = $(this).attr('galerie_id');
            console.log(categorie_curenta);
            console.log(categorie_curenta_galerie);
            $(".poza-galerie").hide();
            $(".poza-galerie[item_category=" + categorie_curenta + "]").fadeIn("slow");
            $(".poza-galerie[poze_category=" + categorie_curenta_galerie + "]").fadeIn("slow");
        });
    });
</script>
@endpush