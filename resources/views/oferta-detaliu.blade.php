@extends('parts.template') @section('content')
<div class = "container camere-container">
    <a class = "pagini-link" href = "/oferte" style = "display:block;"><div class = "pagini">{{ __('site.acasa') }} | {{ __('site.oferte') }} | {{ $oferta->title }}</div></a>
<div class  = "evenimente-title">{{$oferta->title}}</div>
    <div class = "sala-poza" data-aos="flip-up"><img src = "{{ route('thumb', ['width:1000', $oferta->image]) }}" class = "full-width"></div>
    <div class = "evenimente-descriere" style = "text-align-last:left;">{!!$oferta->content!!}</div>

    @if($alteOferte!=NULL)
    <div class = "oferte-contianer">
        @foreach($alteOferte as $item)
        <div class = "overte-element">
            
            <div class = "overte-poza-container">
                <div class = "oferte-overlay">
                   <div class = "asez-text-da">
                       <div class = "text-poza">{{$item->text_poza}}</div>
                    <div class = "tip-container-container">
                        <div class = "tip-camera-imagine"><img class = "full-width-no-object" src = "@if($item->tag =="option1") images/deluxe.svg @elseif($item->tag =="option2") images/deluxe.svg @elseif($item->tag =="option3")images/senior.svg @elseif($item->tag =="option4") images/sky.svg @endif"></div>
                        <div class = "text-poza-tip">@if($item->tag =="option1") All rooms @elseif($item->tag =="option2") Deluxe Room @elseif($item->tag =="option3") Senior Suite @elseif($item->tag =="option4") Sky Senior Suite @endif</div>
                    </div>
                   </div>
               
                </div>
                <div class = "oferte-poza"><img class = "full-width" src = "{{ route('thumb', ['width:500', $item->image]) }}"></div>
                
            </div>
            <div class = "oferte-text-container">
            {{-- <div class = "tip-container"><div class = "tip-imagine"><img class = "full-width-no-object" src = "images/oferte.png"></div><div class  = "tip-text">@if($item->tag =="option1") All rooms @elseif($item->tag =="option2") Deluxe Room @elseif($item->tag =="option3") Senior Suite @elseif($item->tag =="option4") Sky Senior Suite @endif</div></div> --}}
                <div class = "oferte-title-item">{{$item->title}}</div>
                <div class = "overte-description">{!!\Illuminate\Support\Str::limit($item->content,100,$end = '...')!!}</div>
                <a href = "oferta-detaliu/{{$item->id}}" class = "oferte-buton-da-link"><div class = "oferte-buton-da">{{__('site.oferte-buton')}}</div></a>
            </div>
        </div>
        @endforeach
    </div>
    @endif
</div>
@endsection
@push('scripts')
@endpush