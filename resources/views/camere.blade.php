@extends('parts.template') @section('content')
<div class = "container camere-container">
    <a class = "pagini-link" href = "/" style = "display:block;"><div class = "pagini">{{ __('site.acasa') }} | {{ __('site.camere') }}</div></a>
    <div class = "facilitati-camere-mobile">
        <div class  = "evenimente-title">{{ __('site.facilitati-camere') }}</div>
        <div class="swiper-container">
            <div class="swiper-wrapper">
              <div class="swiper-slide swiper-slide-normal">
                <div class = "facilitati-element">
                    <div class = "facilitati-poza"><img src = "images/facilitati-camere1.svg" class = "full-width-no-object"></div>
                    <div class = "facilitati-text facilitati-text-camere">{{ __('site.facilitati-camere1') }}</div>
                </div>
              </div>
              <div class="swiper-slide swiper-slide-normal">
                <div class = "facilitati-element">
                    <div class = "facilitati-poza"><img src = "images/facilitati-camere2.svg" class = "full-width-no-object"></div>
                    <div class = "facilitati-text facilitati-text-camere">{{ __('site.facilitati-camere2') }}</div>
                </div>
              </div>
              <div class="swiper-slide swiper-slide-normal">
                <div class = "facilitati-element">
                    <div class = "facilitati-poza"><img src = "images/facilitati-camere3.svg" class = "full-width-no-object"></div>
                    <div class = "facilitati-text facilitati-text-camere">{{ __('site.facilitati-camere3') }}</div>
                </div>
              </div>
              <div class="swiper-slide swiper-slide-normal">
                <div class = "facilitati-element">
                    <div class = "facilitati-poza"><img src = "images/facilitati-camere4.svg" class = "full-width-no-object"></div>
                    <div class = "facilitati-text facilitati-text-camere">{{ __('site.facilitati-camere4') }}</div>
                </div>
              </div>
              <div class="swiper-slide swiper-slide-normal">
                <div class = "facilitati-element">
                    <div class = "facilitati-poza"><img src = "images/facilitati-camere5.svg" class = "full-width-no-object"></div>
                    <div class = "facilitati-text facilitati-text-camere">{{ __('site.facilitati-camere5') }}</div>
                </div>
              </div>
              <div class="swiper-slide swiper-slide-normal">
                <div class = "facilitati-element">
                    <div class = "facilitati-poza"><img src = "images/facilitati-camere6.svg" class = "full-width-no-object"></div>
                    <div class = "facilitati-text facilitati-text-camere">{{ __('site.facilitati-camere6') }}</div>
                </div>
              </div>
              <div class="swiper-slide swiper-slide-normal">
                <div class = "facilitati-element">
                    <div class = "facilitati-poza"><img src = "images/facilitati-camere7.svg" class = "full-width-no-object"></div>
                    <div class = "facilitati-text facilitati-text-camere">{{ __('site.facilitati-camere7') }}</div>
                </div>
              </div>
            </div>
            <!-- Add Arrows -->
            <div class="swiper-button-next"></div>
            <div class="swiper-button-prev"></div>
          </div>
          <a class="facilitati-link" target="_blank" href="contact"><div class="facilitati-buton">{{ __('site.facilitati-button') }}</div></a>
    </div>

    <div class = "camere">

        @foreach($camere as $item)
        @if($item->id % 2 ==1)
        <div class="about-section1 section1-modificat" data-aos="fade-right">
            <div class="about-section1-left">
            <div class="about-seciton1-title" style = "text-transform:uppercase;">{{$item->name}}</div>
                <div class="about-seciton1-subtitle">{!!\Illuminate\Support\Str::limit($item->description,300,$end = '...')!!}</div>
            <a href="camera-detaliu/{{$item->id}}" style="display:block;"><div class="about-seciton1-button">{{__('site.oferte-buton')}}</div></a>
            </div>
            <div class="about-section1-right">
                <div class="empty-div"></div>
                <div class="section1-image"><img class="full-width" src="{{ route('thumb', ['width:800', $item->image]) }}"></div>
            </div>
        </div>
        @else
        <div class="about-section1-reverse" data-aos="fade-left">
            <div class="about-section1-left-reverse">
            <div class="about-seciton1-title" style = "text-transform:uppercase;">{{$item->name}}</div>
                <div class="about-seciton1-subtitle">{!!\Illuminate\Support\Str::limit($item->description,300,$end = '...')!!}</div>
            <a href="camera-detaliu/{{$item->id}}" style="display:block;"><div class="about-seciton1-button">{{__('site.oferte-buton')}}</div></a>
            </div>
            <div class="about-section1-right">
                <div class="empty-div-reverse"></div>
                <div class="section1-image"><img class="full-width" src="{{ route('thumb', ['width:800', $item->image]) }}"></div>
            </div>
        </div>
        @endif
        @endforeach
</div>
<div class = "facilitati-camere">
    <div class  = "evenimente-title">{{ __('site.facilitati-camere') }}</div>
    <div class = "facilitati-container">
        <div class = "facilitati-element">
            <div class = "facilitati-poza"><img src = "images/facilitati-camere1.svg" class = "full-width-no-object"></div>
            <div class = "facilitati-text facilitati-text-camere">{{ __('site.facilitati-camere1') }}</div>
        </div>
        <div class = "facilitati-element">
            <div class = "facilitati-poza"><img src = "images/facilitati-camere2.svg" class = "full-width-no-object"></div>
            <div class = "facilitati-text facilitati-text-camere">{{ __('site.facilitati-camere2') }}</div>
        </div>
        <div class = "facilitati-element">
            <div class = "facilitati-poza"><img src = "images/facilitati-camere3.svg" class = "full-width-no-object"></div>
            <div class = "facilitati-text facilitati-text-camere">{{ __('site.facilitati-camere3') }}</div>
        </div>
        <div class = "facilitati-element">
            <div class = "facilitati-poza"><img src = "images/facilitati-camere4.svg" class = "full-width-no-object"></div>
            <div class = "facilitati-text facilitati-text-camere">{{ __('site.facilitati-camere4') }}</div>
        </div>
        <div class = "facilitati-element">
            <div class = "facilitati-poza"><img src = "images/facilitati-camere5.svg" class = "full-width-no-object"></div>
            <div class = "facilitati-text facilitati-text-camere">{{ __('site.facilitati-camere5') }}</div>
        </div>
        <div class = "facilitati-element">
            <div class = "facilitati-poza"><img src = "images/facilitati-camere6.svg" class = "full-width-no-object"></div>
            <div class = "facilitati-text facilitati-text-camere">{{ __('site.facilitati-camere6') }}</div>
        </div>
        <div class = "facilitati-element">
            <div class = "facilitati-poza"><img src = "images/facilitati-camere7.svg" class = "full-width-no-object"></div>
            <div class = "facilitati-text facilitati-text-camere">{{ __('site.facilitati-camere7') }}</div>
        </div>
    </div>
    {{-- <a class="facilitati-link" href="servicii"><div class="facilitati-buton">{{ __('site.facilitati-button') }}</div></a> --}}
</div>
@endsection 