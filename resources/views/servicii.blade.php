@extends('parts.template') @section('content')
<div class = "container camere-container">
    <a class = "pagini-link" href = "/" style = "display:block;"><div class = "pagini">{{ __('site.acasa') }} | {{ __('site.servicii') }}</div></a>
    <div class = "servicii-container">
        <div class = "servicii-left" data-aos="fade-right">
            <div class = "servicii-top">
                <div class = "servicii-title">{!!$ServiciiSection1Title->content!!}</div>
                <div class = "servicii-content">{!!$ServiciiSection1Content->content!!}</div>
            </div>
            <div class = "servicii-bottom">
                <img src = "{{ route('thumb', ['width:800', $ServiciiSection1ImageLeft->images]) }}" class = "full-width">
            </div>
        </div>
        <div class = "servicii-right" data-aos="fade-left">
            <div class = "servicii-left-top">
                <img src = "{{ route('thumb', ['width:800', $ServiciiSection1ImageRight->images]) }}" class = "full-width">
            </div>
            <div class = "servicii-left-bottom">
                <div class = "servicii-left-title">{!!$ServiciiSection1Title2->content!!}</div>
                <div class = "servicii-left-content">{!!$ServiciiSection1Content2->content!!}</div>
            </div>
        </div>
    </div>
</div>
<div class="delicii" style = "background-image:url('../images/banner-servicii.jpg');">
    <div class="delicii-overlay">
        <div class="delicii-container">
            <div class="delicii-title">{{ __('site.servicii-banner-title') }}</div>
                <div class="delicii-subtitle">{!!$ServiciiBannerContent->content!!}</div>
            </div>
    </div>
</div>
<div class = "container">
    <div class="about-seciton2 servicii-margin">
        <div class="section2-left" data-aos="fade-right">
            <div class="section2-top">
                <img src="{{ route('thumb', ['width:800', $ServiciiSection2ImageLeft->images]) }}" class="full-width">
            </div>
            <div class="section2-bottom margin-section2-bottom">
                <div class="section2-title">{!!$ServiciiSection2Title->content!!}</div>
                <div class="section2-content">{!!$ServiciiSection2Content->content!!}</div>
            </div>
        </div>
        <div class="section2-right" data-aos="fade-left">
            <img src="{{ route('thumb', ['width:800', $ServiciiSection2ImageRight->images]) }}" class="full-width">
        </div>
    </div>
</div>
@endsection