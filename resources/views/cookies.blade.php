@extends('parts.template') @section('content')
<div class = "container camere-container">
    <a class = "pagini-link" href = "" style = "display:block;"><div class = "pagini">{{ __('site.acasa') }} | {{ __('site.cookies') }}</div></a>
    <div class = "texte-container">
        <div class = "texte-title">{{ __('site.cookies') }}</div>
        <div class = "texte-contianer">{!!strip_tags($cookiesText->content,"<p>")!!}</div>
    </div>
</div>
@endsection