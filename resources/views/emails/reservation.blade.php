@component('mail::message')
# Aveti o rezervare noua

@component('mail::panel')
<div style="width:100%; text-align:center; font-size:30px; font-height:bold;">
Rezervare noua
</div>

Nume: {{$message['nume']}}<br>
Telefon: {{$message['telefon']}}<br>
Data: {{$message['data']}}<br>
Numar de adulti: {{$message['adulti']}}<br>
Numar de copii: {{$message['copii']}}<br>
Camera: {{$message['camera']}}<br>
Cod Promotional: {{$message['cod']}}<br>


@endcomponent

Multumim,<br>
Echipa Hotel Opera
@endcomponent
