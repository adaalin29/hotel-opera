@extends('parts.template') @section('content')
<div class = "container camere-container">
    <a class = "pagini-link" href = "/" style = "display:block;"><div class = "pagini">{{ __('site.acasa') }} | Contact</div></a>
    <div class  = "evenimente-title">{{ __('site.contact') }}</div>
    <div class = "contact-subtitle">{!!$ContactContent->content!!}</div>
    <form class = "contact-form">
        <div class = "contact-form-top">
            <div class = "contact-element">
                <div class = "contact-text">{{__('site.nume-complet')}}</div>
                <input type="text" id="fname" name="name" class = "contact-input">
            </div>
            <div class = "contact-element">
                <div class = "contact-text">Email</div>
                <input type="email" id="femail" name="email" class = "contact-input">
            </div>
            <div class = "contact-element">
                <div class = "contact-text">{{__('site.telefon')}}</div>
                <input type="number" id="ftelefon" name="phone" class = "contact-input">
            </div>
        </div>
        <div class = "contact-form-bottom">
            <div class = "contact-text">{{__('site.message')}}</div>
            <textarea name="message"></textarea>
        </div>
        <div class = "termeni-container">
            <div class = "termeni">
                <input type="checkbox" id="accept-privacy" name="termeni" value="checkbox" style="vertical-align: middle; text-align: left; width: 20px;">
            <div class = "contact-checkbox-text">{{__('site.check')}} <a class = "terms-text" href = "terms">{{__('site.terms')}}</a></div>
            </div>
        </div>
        <button class="btn-send-message about-seciton1-button" type="submit">{{__('site.trimite')}}</button>
    </form>
</div>
<div class = "map-details"></div>
<div class = "map">
    <div class = "map-container">
        <div class = "map-logo"><img class = "full-width-no-object" src = "images/map-logo.png"></div>
        <div class = "map-title">Contact</div>
        <div class = "map-location">{{setting('site.locatie')}}</div>
        <a href = "tel:{{setting('site.telefon')}}" style = "display:block;"><div class = "map-location" style = "margin-top:20px">{{setting('site.telefon')}}</div></a>
        <a href = "tel:{{setting('site.telefon2')}}" style = "display:block;"><div class = "map-location" style = "margin-top:20px">{{setting('site.telefon')}}</div></a>
        <a href = "tel:{{setting('site.fax')}}" style = "display:block;"><div class = "map-location">{{setting('site.fax')}}</div></a>
        <a href = "mailto:{{setting('site.email')}}" style = "display:block;"><div class = "map-location" style = "margin-top:20px">{{setting('site.email')}}</div></a>
        <a class="facilitati-link" target="_blank" href="http://www.google.com/maps/place/{{setting('site.latitudine')}},{{setting('site.longitudine')}}"><div class="facilitati-buton" style = "margin-top:20px">{{__('site.intra')}}</div></a>
    </div>
    <div id="map-canvas" style="height:100%"></div>
</div>
@endsection
@push('scripts')
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyCmM1P-D5Zka0kPEbZSrsR90gpBlDxgm18"></script>
<script>
    function initialize() {

// 				var geocoder;

//         var address = "{{setting('site.adresa')}}";

// # Get marker data

var defaultMarkerLat = "{{setting('site.latitudine')}}";

var defaultMarkerLng = "{{setting('site.longitudine')}}";

var markerImg = '../images/marker.png';

var markerTitle = "{{setting('site.title')}}";



// # Show map

var myLatlng = new google.maps.LatLng(defaultMarkerLat, defaultMarkerLng);

var mapOptions = {

    zoom: 17,

    center: myLatlng,

    scrollwheel: false,

    mapTypeId: google.maps.MapTypeId.ROADMAP,

    disableDefaultUI: true

}

var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

// # Show marker

var marker = new google.maps.Marker({



    position: myLatlng,

    map: map,

    // 					icon:{markerImg} ,
    icon: {
        url: "images/marker.png",
        scaledSize: new google.maps.Size(40, 50)
    },

    title: markerTitle
});

}

google.maps.event.addDomListener(window, 'load', initialize);

function mapsSelector() {
        if /* if we're on iOS, open in Apple Maps */ ((navigator.platform.indexOf("iPhone") != -1) ||
            (navigator.platform.indexOf("iPad") != -1) ||
            (navigator.platform.indexOf("iPod") != -1))
            window.open(
                "http://maps.apple.com/?ll={{setting('site.latitude')}},{{setting('site.longitude')}}"
            );
        //      window.open("https://maps.google.com/maps?daddr={{setting('contact.contact-latitudine')}},{{setting('contact.contact-longitudine')}}&amp;ll=");
        else /* else use Google */
            window.open(
                "https://maps.google.com/maps?daddr={{setting('site.latitude')}},{{setting('site.longitude')}}&amp;ll="
            );
    }
</script>
<script>
     document.addEventListener("DOMContentLoaded", function () {
    $.ajaxSetup({

      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
      }
    });
    var $formContact = $('.contact-form');
    $('.btn-send-message').on('click', function (event) {
      event.preventDefault();
      $.ajax({
        method: 'POST',
        url: '{{ action("ContactController@send_message") }}',
        data: $formContact.serializeArray(),
        context: this,
        async: true,
        cache: false,
        dataType: 'json'
      }).done(function (res) {
        console.log(res);
        if (res.success == true) {
          $.notify(res.successMessage, "success");
          setTimeout(function () {
            window.location.reload();
          
          }, 4000);
        } else {
          var eroare = res.error;
        for (var i = 0; i < eroare.length; i++) {
          eroare[i] = eroare[i] + "\n";
        }
          $.notify(res.error, {
            type: "error",
            breakNewLines: true,
            gap:2
          });
        }
      });
      return;
    });

  });
</script>
<script>
  $(document).ready(function(){
    if(screen.width<=1024)
      $(".map-container").appendTo(".map-details");
  });
</script>
@endpush