<!DOCTYPE html>
<html>

<head>
  <base href="{{ URL::to('/') }}" />
  <title>Hotel Opera</title>
  <meta charset="utf-8" />
  <meta name="description" content="@yield('description')" />
  <meta name="keywords" content="@yield('keywords')" />
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <link href="css/style.css" rel="stylesheet" type="text/css" />
  <link href="css/responsive.css" rel="stylesheet" type="text/css" />
  <link href="css/datepicker.min.css" rel="stylesheet" type="text/css" />
  <link href="css/swiper.min.css" rel="stylesheet" type="text/css" />
  <link href="css/fancybox.css" rel="stylesheet" type="text/css" />
  <link href="css/aos.css" rel="stylesheet" type="text/css" />
  <link href="css/animate.css" rel="stylesheet" type="text/css" />
  @stack('styles')
</head>

<body>

  <div id="page">
    <div class = "sidenav">
      <div class = "back-button"><div class = "back-button-image"><img class = "full-width-no-object" src = "images/back-button.svg"></div><div class = "back-button-text">{{__('site.back')}}</div></div>
        <div class = "sidenav-container">
          <a href = ""><div class = "sidenav-logo"><img class = "full-width-no-object" src = "images/header.svg"></div></a>
          <div class = "sidenav-elemente">
            <a class = "sidenav-link" href = "camere"><div class = "sidenav-element-container"><div class = "sidenav-element">{{ __('site.camere') }}</div>@if(Request::path()=='camere') <div class = "sidenav-linie sidenav-linie1"></div> @endif</div></a>
            <a class = "sidenav-link" href = "restaurant"><div class = "sidenav-element-container"><div class = "sidenav-element">{{ __('site.restaurant') }}</div>@if(Request::path()=='restaurant') <div class = "sidenav-linie sidenav-linie2"></div> @endif</div></a>
            <a class = "sidenav-link" href = "oferte"><div class = "sidenav-element-container"><div class = "sidenav-element">{{ __('site.oferte') }}</div>@if(Request::path()=='oferte') <div class = "sidenav-linie sidenav-linie3"></div> @endif</div></a>
            <a class = "sidenav-link" href = "evenimente"><div class = "sidenav-element-container"><div class = "sidenav-element">{{ __('site.evenimente') }}</div>@if(Request::path()=='evenimente') <div class = "sidenav-linie sidenav-linie4"></div> @endif</div></a>
            <a class = "sidenav-link" href = "galerie"><div class = "sidenav-element-container"><div class = "sidenav-element">{{ __('site.galerie') }}</div>@if(Request::path()=='galerie') <div class = "sidenav-linie sidenav-linie5"></div> @endif</div></a>
            {{-- <a class = "sidenav-link" href = "opera"><div class = "sidenav-element-container"><div class = "sidenav-element">{{ __('site.opera') }}</div>@if(Request::path()=='opera') <div class = "sidenav-linie sidenav-linie6"></div> @endif</div></a> --}}
            <a class = "sidenav-link" href = "contact"><div class = "sidenav-element-container"><div class = "sidenav-element">Contact</div>@if(Request::path()=='contact') <div class = "sidenav-linie sidenav-linie7"></div> @endif</div></a>
          </div>
          {{-- <div class = "sidenav-location">{{setting('site.locatie')}}</div>
          <div class = "sidenav-numbers">
            <a href = "tel:{{setting('site.telefon')}}" class = "sidenav-telephone"><div class = "sidenav-tel">Tel {{setting('site.telefon')}}</div></a>
            <a href = "tel:{{setting('site.telefon2')}}" class = "sidenav-telephone"><div class = "sidenav-tel">Tel {{setting('site.telefon2')}}</div></a>
            <a href = "tel:{{setting('site.fax')}}" class = "sidenav-telephone"><div class = "sidenav-tel">Fax {{setting('site.fax')}}</div></a>
          </div> --}}
          <div class = "sidenav-social">
            <a href = "{{setting('site.facebook-link')}}" class = "sidenav-social-link"><div class = "sidenav-social-element"><img class = "full-width-no-object" src = "images/facebook.svg"></div></a>
            <a href = "{{setting('site.instagram')}}" class = "sidenav-social-link"><div class = "sidenav-social-element"><img class = "full-width-no-object" src = "images/twitter.svg"></div></a>
            <a href = "{{setting('site.tripadvisor-link')}}" class = "sidenav-social-link"><div class = "sidenav-social-element"><img class = "full-width-no-object" src = "images/tripadvisor.svg"></div></a>
          </div>
      </div>
    </div>
    <div class = "reservation-overlay">
      
    </div>
   {{-- @if($news[0]['visible']==1)
   <div class = "pop-up-overlay">
    <div class = "container pop-container">
      <div class = "close-pop-up-container"><div class = "close-pop-up"><img src = "images/close.svg" class = "full-width-no-object"></div></div>
      <div class = "pop-up-content">
        <div class = "pop-up-title">{{$news[0]['title']}}</div>
        <div class = "pop-up-description">{!!$news[0]['description']!!}</div>
        <a class = "delicii-link" href = "contact"><div class = "delicii-buton">{{ __('site.oferte-buton') }}</div></a>
    </div>
    </div>
  </div>
   @endif --}}
    @include('parts.header')
    <main id="content">
      @if($news[0]['visible']==1)
      <div class = "pop-up-mic">
        <div class = "pop-up-mic-close-container"><div class = "pop-up-mic-close"><img src = "images/close-white.svg" class = "full-width-no-object"></div></div>
        <div class = "pop-up-content">
          <div class = "pop-up-imagine"><img src = "images/pop-up.JPG" class = "full-width"></div>
          <div class = "pop-up-text">
            <div class = "pop-up-logo"><img src = "images/pop-up-logo.png" class = "full-width-no-object"></div>
            <div class = "pop-up-mic-subtitle">{!!$news[0]['description']!!}</div>
            <a class = "delicii-link-pop-up" target = "_blank" href = "https://booking.operahotel.ro/Book/BookingEngine.aspx?hotelId=5747&language=ro"><div class = "delicii-buton-pop-up">{{ __('site.oferte-buton-da') }}</div></a>
          </div>
        </div>
      </div>
      @endif
      @if($news[0]['visible']==1)
      <div class = "pop-up-button animated  swing">
        <img src = "images/pop.svg" class = "full-width-no-border">
      </div>
      @endif
      @yield('content')
    </main>
    @include('parts.footer')
  </div>
  <button class="scroll-up"> <img src="images/arrow.png"> </button>

  <script src="js/jquery.js" type="text/javascript"></script>
  <script src="js/common.js" type="text/javascript"></script>
  <script src="js/cookies.js" type="text/javascript"></script>
  <script src="js/datepicker.min.js" type="text/javascript"></script>
  <script src="js/datepicker.en.js" type="text/javascript"></script>
  <script src="js/swiper.min.js" type="text/javascript"></script>
  <script src="js/aos.js" type="text/javascript"></script>
  <script src="js/fancybox.js" type="text/javascript"></script>
  <script src="js/datepicker.ro.js" type="text/javascript"></script>
  <script src="js/notify.min.js" type="text/javascript"></script>

  @stack('scripts')
</body>

</html>