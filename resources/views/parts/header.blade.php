<header id="header">
    <div class="container" style = "height:100%;">
        <div class = "header-container">
              <div class="topmenu desktop-hidden">
                <div class="bar1"></div>
                <div class="bar2"></div>
                <div class="bar3"></div>
            </div>
            <a class = "align-mobile" href =""><div class = "header-logo"><img src = "images/header.svg" class = "full-width"></div></a>
            <div class = "header-menu">
                <a class = "one @if(Request::path() == "camere") one-active @endif" href = "camere"><div class = "header-element">{{ __('site.camere') }}</div></a>
                <a class = "two @if(Request::path() == "restaurant") two-active @endif" href = "restaurant" ><div class = "header-element">{{ __('site.restaurant') }}</div></a>
                <a class = "three @if(Request::path() == "oferte") three-active @endif" href = "oferte"><div class = "header-element">{{ __('site.oferte') }}</div></a>
                <a class = "four @if(Request::path() == "evenimente") four-active @endif" href = "evenimente"><div class = "header-element">{{ __('site.evenimente') }}</div></a>
                <a class = "five @if(Request::path() == "galerie") five-active @endif" href = "galerie"><div class = "header-element">{{ __('site.galerie') }}</div></a>
                {{-- <a class = "six @if(Request::path() == "opera") six-active @endif" href = "opera"><div class = "header-element">{{ __('site.opera') }}</div></a> --}}
                <a class = "seven @if(Request::path() == "contact") seven-active @endif" href = "contact" ><div class = "header-element">Contact</div></a>
                <linie></linie>
            </div>
          <div class = "reservation-button-header">{{__('site.rezerva-online')}}</div>
          <div class = "rezerva-button desktop-hidden"><img class = "full-width-no-object" src = "images/calendar-white.svg"></div>
        </div>
    </div>
    <div class = "rezervation">
        <div class = "container-rezervari">
          <div class = "close-reservation desktop-hidden"><div class = "close-pop-up close-pop-up-reservation"><img src = "images/close-white.svg" class = "full-width-no-object"></div></div>

        <div class = "desktop-hidden rezerva-title">{{__('site.rezerva')}}</div>





            <div class = "reservation-form">
                <div class = "reservation-data reservation-element">
                    <div class = "reservation-text">{{ __('site.data-sosire') }}</div>
                    <input onfocus="blur();" id = "data-sosire-input" name = "data-sosire" type='text' class='datepicker-here'
                        data-date-format="yyyy-mm-dd"
                        data-language="ro">
                        <img class = 'reservation-image' src = "images/calendar.svg">
                </div>

                <div class = "reservation-data reservation-element">
                  <div class = "reservation-text">{{ __('site.data-plecare') }}</div>
                  <input onfocus="blur();" id = "data-plecare-input" name = "data-plecare" type='text' class='datepicker-here'
                      data-date-format="yyyy-mm-dd"
                      data-language="ro">
                      <img class = 'reservation-image' src = "images/calendar.svg">
              </div>

                {{-- <div class = "reservation-adults reservation-element">
                    <div class = "reservation-text">{{ __('site.adulti') }}</div>
                    <select name = "adulti">
                        <option value="1">1</option>
                        <option selected="selected" value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                      </select>
                        <img class = 'reservation-image-arrow' src = "images/arrow.svg">
                </div>
                <div class = "reservation-adults reservation-element">
                    <div class = "reservation-text">{{ __('site.copii') }}</div>
                    <select name = "copii">
                        <option selected="selected" value="0">0</option>
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                      </select>
                        <img class = 'reservation-image-arrow' src = "images/arrow.svg">
                </div>

                <div class = "reservation-cod reservation-element">
                    <div class = "reservation-text">{{ __('site.cod') }}</div>
                    <input name = "cod" type='text'>
                </div>

                <div class = "reservation-name reservation-element">
                    <div class = "reservation-text">{{ __('site.nume') }}</div>
                    <input  name = "nume" type='text'>
                </div>
                <div class = "reservation-phone reservation-element">
                    <div class = "reservation-text">{{ __('site.numar') }}</div>
                    <input name = "telefon" type='number'>
                </div>
                <div class = "reservation-camera reservation-element">
                    <div class = "reservation-text">{{ __('site.camera') }}</div>
                    <select name = "camera">
                    <option  value="">{{__('site.select-room')}}</option>
                        @foreach($rooms as $item)
                    <option value="{{$item->name}}" @if(isset($camera) && $item->slug==$camera->slug) selected @endif>{{$item->name}}</option>
                        @endforeach
                      </select>
                        <img class = 'reservation-image-arrow' src = "images/arrow.svg">
                </div> --}}
                <button  class = "reservation-button">{{ __('site.valabilitate') }}</button>
              </div>

              
        </div>
    </div>
</header>
@push('scripts')
<script>
  $('.reservation-button').click(function(){
    var dataSosire = $("#data-sosire-input").val(); 
    var dataPlecare = $("#data-plecare-input").val();
    
    if(dataSosire != "" || dataPlecare!= ""){
      // console.log(dataSosire);
      window.open("https://booking.operahotel.ro/Book/BookingEngine.aspx?hotelId=5747&language=ro&arrivalDate=" +dataSosire+ "&departureDate="+dataPlecare +"&cf_id=16543");
    }
    else{
      $.notify("{{__('site.introdu-date')}}", "error");
    }




    // if(($('#data-sosire-input').is(":empty")) || ($('#data-plecare-input').is(":empty"))){
    //   $.notify("{{__('site.introdu-date')}}", "error");
    // }
    // else{
    //   $.notify("{{__('site.introdu-date')}}", "succes");
    // }
  });
</script>
{{-- <script>
       document.addEventListener("DOMContentLoaded", function () {
    $.ajaxSetup({

      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
      }
    });
    var $formContact = $('.reservation-form');
    $('.reservation-button').on('click', function (event) {
      event.preventDefault();
      $.ajax({
        method: 'POST',
        url: '{{ action("ContactController@send_reservation") }}',
        data: $formContact.serializeArray(),
        context: this,
        async: true,
        cache: false,
        dataType: 'json'
      }).done(function (res) {
        console.log(res);
        if (res.success == true) {
          $.notify(res.successMessage, "success");
          setTimeout(function () {
            window.location.reload();
          
          }, 4000);
        } else {
          var eroare = res.error;
        for (var i = 0; i < eroare.length; i++) {
          eroare[i] = eroare[i] + "\n";
        }
          $.notify(res.error, {
            type: "error",
            breakNewLines: true,
            gap:2
          });
        }
      });
      return;
    });

  });
</script> --}}
@endpush