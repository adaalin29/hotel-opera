<footer id="footer">
    <div class = "container">
        <div class = "footer-container">
            <div class = "footer-section-container">
                <div class = "footer-section">
                    <div class = "footer-title">{{ __('site.informatii') }}</div>
                    {{-- <a href = "about" style = "display:block;"><div class = "footer-text">{{ __('site.about') }}</div></a> --}}
                    <a href = "camere" style = "display:block;"><div class = "footer-text">{{ __('site.camere') }}</div></a>
                    <a href = "servicii" style = "display:block;"><div class = "footer-text">{{ __('site.servicii') }}</div></a>
                    <a href = "oferte" style = "display:block;"><div class = "footer-text">{{ __('site.oferte') }}</div></a>
                </div>
                <div class = "footer-section section-middle">
                    <a href = "galerie" style = "display:block;"><div class = "footer-text">{{ __('site.galerie') }}</div></a>
                    <a href = "evenimente" style = "display:block;"><div class = "footer-text">{{ __('site.evenimente') }}</div></a>
                    <a href = "contact" style = "display:block;"><div class = "footer-text">Contact</div></a>
                </div>
                <div class = "footer-section">
                    <div class = "footer-title">{{ __('site.more') }}</div>
                    <a href = "politica" style = "display:block;"><div class = "footer-text">{{ __('site.politica') }}</div></a>
                    <a href = "cookies" style = "display:block;"><div class = "footer-text">{{ __('site.cookies') }}</div></a>
                    <a href = "termeni" style = "display:block;"><div class = "footer-text">{{ __('site.terms') }}</div></a>
                </div>
            </div>
            <div class = "footer-section-container">
                <div class = "footer-logo"><a href = "" style = "display:block;"><img src = "images/footer_loog.svg" class = "full-width"></a></div>
            </div>
            <div class = "footer-section-container-mobile">
                <a href = "politica" style = "display:block;"><div class = "footer-text">{{ __('site.politica') }}</div></a>
                    <a href = "cookies" style = "display:block;"><div class = "footer-text">{{ __('site.cookies') }}</div></a>
                    <a href = "termeni" style = "display:block;"><div class = "footer-text">{{ __('site.terms') }}</div></a>
            </div>
            <div class = "footer-section-container">
                <div class = "footer-section-reverse">
                    <div class = "multi-language">
                        <div class = "multilanguage-text">
                            @if(Config::get('app.locale') == 'ro')
                            <div class = "multilanguage-text-text">Romana</div>
                            @endif
                            @if(Config::get('app.locale') == 'en')
                            <div class = "multilanguage-text-text">English</div>
                            @endif
                            @if(Config::get('app.locale') == 'de')
                            <div class = "multilanguage-text-text">Deusch</div>
                            @endif
                            <div class = "multilanguage-arrow"><img src = "images/multilanguage-arrow.svg" class = "full-width-no-object"></div>
                        </div>
                        <div class = "multilanguage-container">
                            <a href = "locale/ro" class = "display:block"><div class = "multilanguage-lang-text">RO</div></a>
                            <a href = "locale/en" class = "display:block"><div class = "multilanguage-lang-text">EN</div></a>
                            <a href = "locale/de" class = "display:block"><div class = "multilanguage-lang-text">DE</div></a>
                        </div>
                    </div>
                    <div class = "social-contianer">
                        <a href = "{{setting('site.facebook-link')}}" style = "display:block;"><div class = "social-item"><img src = "images/facebook.svg" class = "full-width" style = "object-fit:initial;"></div></a>
                        <a href = "{{setting('site.instagram')}}" style = "display:block;"><div class = "social-item"><img src = "images/twitter.svg" class = "full-width" style = "object-fit:initial;"></div></a>
                        <a href = "{{setting('site.tripadvisor-link')}}" style = "display:block;"><div class = "social-item"><img src = "images/tripadvisor.svg" class = "full-width" style = "object-fit:initial;"></div></a>
                    </div>
                    <div class=  "copy-text">{{ __('site.copy') }}</div>
                </div>
            </div>
        </div>
    </div>
 </footer>