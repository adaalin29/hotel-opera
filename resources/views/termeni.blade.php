@extends('parts.template') @section('content')
<div class = "container camere-container">
    <a class = "pagini-link" href = "" style = "display:block;"><div class = "pagini">{{ __('site.acasa') }} | {{ __('site.terms') }}</div></a>
    <div class = "texte-container">
        <div class = "texte-title">{{ __('site.terms') }}</div>
        <div class = "texte-contianer">{!!strip_tags($termeniText->content,"<p>")!!}</div>
    </div>
</div>
@endsection