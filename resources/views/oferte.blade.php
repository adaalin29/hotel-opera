@extends('parts.template') @section('content')
<div class = "container camere-container">
    <a class = "pagini-link" href = "/" style = "display:block;"><div class = "pagini">{{ __('site.acasa') }} | {{ __('site.oferte') }}</div></a>
    <div class = "servicii-container">
        <div class = "servicii-left" data-aos="fade-right">
            <div class = "servicii-top">
                <div class = "servicii-title">{!!$OferteSection1Title1->content!!}</div>
                <div class = "servicii-content">{!!$OferteSection1Content1->content!!}</div>
            </div>
            <div class = "servicii-bottom">
                <img src = "{{ route('thumb', ['width:800', $OferteSection1ImageLeft->images]) }}" class = "full-width">
            </div>
        </div>
        <div class = "servicii-right" data-aos="fade-left">
            <div class = "servicii-left-top">
                <img src = "{{ route('thumb', ['width:800', $OferteSection1ImageRight->images]) }}" class = "full-width">
            </div>
            <div class = "servicii-left-bottom">
                <div class = "servicii-left-title">{!!$OferteSection1Title2->content!!}</div>
                <div class = "servicii-left-content">{!!$Content2->content!!}</div>
            </div>
        </div>
    </div>
    @if($oferte!=NULL)
<div class = "evenimente-title">{{__('site.oferte-valabile')}}</div>
    <div class = "oferte-contianer">
        @foreach($oferte as $item)
        <div class = "overte-element">
            
            <div class = "overte-poza-container">
                <div class = "oferte-overlay">
                   <div class = "asez-text-da">
                       <div class = "text-poza">{{$item->text_poza}}</div>
                    <div class = "tip-container-container">
                        <div class = "tip-camera-imagine"><img class = "full-width-no-object" src = "@if($item->tag =="option1") images/deluxe.svg @elseif($item->tag =="option2") images/deluxe.svg @elseif($item->tag =="option3")images/senior.svg @elseif($item->tag =="option4") images/sky.svg @endif"></div>
                        <div class = "text-poza-tip">@if($item->tag =="option1") All rooms @elseif($item->tag =="option2") Deluxe Room @elseif($item->tag =="option3") Senior Suite @elseif($item->tag =="option4") Sky Senior Suite @endif</div>
                    </div>
                   </div>
               
                </div>
                <div class = "oferte-poza"><img class = "full-width" src = "{{ route('thumb', ['width:500', $item->image]) }}"></div>
                
            </div>
            <div class = "oferte-text-container">
            {{-- <div class = "tip-container"><div class = "tip-imagine"><img class = "full-width-no-object" src = "images/oferte.png"></div><div class  = "tip-text">@if($item->tag =="option1") All rooms @elseif($item->tag =="option2") Deluxe Room @elseif($item->tag =="option3") Senior Suite @elseif($item->tag =="option4") Sky Senior Suite @endif</div></div> --}}
                <div class = "oferte-title-item">{{$item->title}}</div>
                <div class = "overte-description">{!!\Illuminate\Support\Str::limit($item->content,100,$end = '...')!!}</div>
                <a href = "oferta-detaliu/{{$item->id}}" class = "oferte-buton-da-link"><div class = "oferte-buton-da">{{__('site.oferte-buton')}}</div></a>
            </div>
        </div>
        @endforeach
    </div>
    @endif
    {{-- @if($oferte!=NULL)
    <div class = "center-title">{{__('site.oferte-title')}}</div>
    <div class = "oferte-container">
        @foreach($oferte as $item)
        <div class = "oferte-item">
                <div class = "oferte-buton">
                    <div class = "oferte-buton-interior">
                        <div class = "oferte-buton-text">{{__('site.oferte-buton')}}</div>
                        <div class = "plus-sign"><img class = "full-width-no-object" src = "images/plus.svg"></div>
                        <div class = "minus-sign"><img class = "full-width-no-object" src = "images/minus.svg"></div>
                    </div>
                </div>
            <div class = "dunga-oferte"></div>
        <div class = "oferte-title">{{$item->title}}</div>
        <div class = "oferte-text">{!!$item->content!!}</div>
        <div class = "buton-oferte-mutare">
        </div>
        </div>
        @endforeach
    </div>
    @endif --}}
</div>
@endsection
@push('scripts')




{{-- <script>
    $('.oferte-buton').click(function () {
            if ($(this).parent().find($('.oferte-text')).hasClass('element-afisat')) {
                $(this).parent().find($('.oferte-text')).removeClass('element-afisat');
                $(this).parent().find($('.oferte-text')).css('max-height','80px');
                $(this).find('.oferte-buton-interior').find('.plus-sign').css('top', '30%');
                $(this).find('.oferte-buton-interior').find('.minus-sign').css('top', '-100%');
            } else {

                $('.oferte-text').removeClass('element-afisat');
                $(this).parent().find($('.oferte-text')).addClass('element-afisat');
                $(this).parent().find($('.oferte-text')).css('max-height','1000px');
                $(this).find('.oferte-buton-interior').find('.plus-sign').css('top', '-100%');
                $(this).find('.oferte-buton-interior').find('.minus-sign').css('top', '30%');
            }
        });
</script> --}}
@endpush