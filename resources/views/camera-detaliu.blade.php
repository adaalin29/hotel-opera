@extends('parts.template') @section('content')
<div class = "container camere-container">
    <a class = "pagini-link-lung" href = "camere" style = "display:block;"><div class = "pagini">{{ __('site.acasa') }} | {{ __('site.camere') }} | {{$camera->name}}</div></a>
    <div class  = "evenimente-title">{{$camera->name}}</div>
    <div class = "sala-poza" data-aos="flip-up"><img src = "{{ route('thumb', ['width:1000', $camera->image]) }}" class = "full-width"></div>
    <div class = "camera-descriere">{!!$camera->description!!}</div>
    {{-- <a class = "delicii-link" target = "_blank" href = "contact"><div class = "delicii-buton">{{ __('site.rezerva') }}</div></a> --}}
</div>
<div class = "galerie-swiper">
    @if($camera->gallery!=NULL)
    <div class = "camere-swiper">
        <div class = "camere-swiper-container">
            <div class="swiper-container">
                <div class="swiper-wrapper">
                    @foreach($camera->gallery as $item)
                    <div class="swiper-slide">
                        <div class = "index-camere">
                            <a class="fancybox-width" data-fancybox="gallery" href="{{ route('thumb', ['width:1920', $item]) }}">
                                <img class = "full-width" src = "{{ route('thumb', ['width:1000', $item]) }}">
                            </a>
                        </div>
                    </div>
                    @endforeach
                </div>
                <!-- Add Pagination -->
                <div class="swiper-pagination"></div>
                <!-- Add Arrows -->
                <div class="swiper-button-next"></div>
                <div class="swiper-button-prev"></div>
                </div>
        </div>
    </div>
    @endif
</div>
<div class = "container">
    <div class  = "evenimente-title">{{__('site.informatii-rezervare')}}</div>
    <div class = "camera-descriere" data-aos="fade-left">{!!$camera->informations!!}</div>
    <div class = "facilitati-hotel">
        <div class  = "evenimente-title">{{ __('site.facilitati') }}</div>
        <div class = "facilitati-container">
            <div class = "facilitati-element">
                <div class = "facilitati-poza"><img src = "images/facilitati1.svg" class = "full-width"></div>
                <div class = "facilitati-text">{{ __('site.facilitati1') }}</div>
            </div>
            <div class = "facilitati-element">
                <div class = "facilitati-poza"><img src = "images/facilitati2.svg" class = "full-width"></div>
                <div class = "facilitati-text">{{ __('site.facilitati2') }}</div>
            </div>
            <div class = "facilitati-element">
                <div class = "facilitati-poza"><img src = "images/facilitati3.svg" class = "full-width"></div>
                <div class = "facilitati-text">{{ __('site.facilitati3') }}</div>
            </div>
            <div class = "facilitati-element">
                <div class = "facilitati-poza"><img src = "images/facilitati4.svg" class = "full-width"></div>
                <div class = "facilitati-text">{{ __('site.facilitati4') }}</div>
            </div>
            <div class = "facilitati-element">
                <div class = "facilitati-poza"><img src = "images/facilitati5.svg" class = "full-width"></div>
                <div class = "facilitati-text">{{ __('site.facilitati5') }}</div>
            </div>
            <div class = "facilitati-element">
                <div class = "facilitati-poza"><img src = "images/facilitati6.svg" class = "full-width"></div>
                <div class = "facilitati-text">{{ __('site.facilitati6') }}</div>
            </div>
            <div class = "facilitati-element">
                <div class = "facilitati-poza"><img src = "images/facilitati7.svg" class = "full-width"></div>
                <div class = "facilitati-text">{{ __('site.facilitati7') }}</div>
            </div>
        </div>
    </div>
    <div class = "facilitati-camere-mobile" style = "margin-top:40px;">
        <div class  = "evenimente-title">{{ __('site.facilitati') }}</div>
        <div class="swiper-container">
            <div class="swiper-wrapper">

              <div class="swiper-slide swiper-slide-normal">
                <div class = "facilitati-element">
                    <div class = "facilitati-poza"><img src = "images/facilitati1.svg" class = "full-width-no-object"></div>
                    <div class = "facilitati-text facilitati-text-camere">{{ __('site.facilitati1') }}</div>
                </div>
              </div>

              <div class="swiper-slide swiper-slide-normal">
                <div class = "facilitati-element">
                    <div class = "facilitati-poza"><img src = "images/facilitati2.svg" class = "full-width-no-object"></div>
                    <div class = "facilitati-text facilitati-text-camere">{{ __('site.facilitati2') }}</div>
                </div>
              </div>
              <div class="swiper-slide swiper-slide-normal">
                <div class = "facilitati-element">
                    <div class = "facilitati-poza"><img src = "images/facilitati3.svg" class = "full-width-no-object"></div>
                    <div class = "facilitati-text facilitati-text-camere">{{ __('site.facilitati3') }}</div>
                </div>
              </div>
              <div class="swiper-slide swiper-slide-normal">
                <div class = "facilitati-element">
                    <div class = "facilitati-poza"><img src = "images/facilitati4.svg" class = "full-width-no-object"></div>
                    <div class = "facilitati-text facilitati-text-camere">{{ __('site.facilitati4') }}</div>
                </div>
              </div>
              <div class="swiper-slide swiper-slide-normal">
                <div class = "facilitati-element">
                    <div class = "facilitati-poza"><img src = "images/facilitati5.svg" class = "full-width-no-object"></div>
                    <div class = "facilitati-text facilitati-text-camere">{{ __('site.facilitati5') }}</div>
                </div>
              </div>
              <div class="swiper-slide swiper-slide-normal">
                <div class = "facilitati-element">
                    <div class = "facilitati-poza"><img src = "images/facilitati6.svg" class = "full-width-no-object"></div>
                    <div class = "facilitati-text facilitati-text-camere">{{ __('site.facilitati6') }}</div>
                </div>
              </div>
              <div class="swiper-slide swiper-slide-normal">
                <div class = "facilitati-element">
                    <div class = "facilitati-poza"><img src = "images/facilitati7.svg" class = "full-width-no-object"></div>
                    <div class = "facilitati-text facilitati-text-camere">{{ __('site.facilitati7') }}</div>
                </div>
              </div>

            </div>
            <!-- Add Arrows -->
            <div class="swiper-button-next"></div>
            <div class="swiper-button-prev"></div>
          </div>
          <a class="facilitati-link" target="_blank" href="contact"><div class="facilitati-buton">{{ __('site.facilitati-button') }}</div></a>
    </div>
</div>
@endsection
@push('scripts')
<script>
    $(document).ready(function(){
        var slidesMobileElemente=3;
            if(screen.width<=768) {
                slidesMobileElemente=1;
            }
            var swiper = new Swiper('.camere-swiper-container .swiper-container', {
            centeredSlides: true,
            slidesPerView: slidesMobileElemente,
            spaceBetween: 200,
            slidesPerGroup: 1,
            loop: true,
            loopFillGroupWithBlank: true,
                navigation: {
                    nextEl: '.swiper-button-next',
                    prevEl: '.swiper-button-prev',
                },
                
        });
    });
</script>
@endpush 