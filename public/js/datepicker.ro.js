;(function ($) { $.fn.datepicker.language['ro'] = {
    days: ['DuminicÄ', 'Luni', 'MarĹŁi', 'Miercuri', 'Joi', 'Vineri', 'SĂ˘mbÄtÄ'],
    daysShort: ['Dum', 'Lun', 'Mar', 'Mie', 'Joi', 'Vin', 'SĂ˘m'],
    daysMin: ['D', 'L', 'Ma', 'Mi', 'J', 'V', 'S'],
    months: ['Ianuarie','Februarie','Martie','Aprilie','Mai','Iunie','Iulie','August','Septembrie','Octombrie','Noiembrie','Decembrie'],
    monthsShort: ['Ian', 'Feb', 'Mar', 'Apr', 'Mai', 'Iun', 'Iul', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec'],
    today: 'Azi',
    clear: 'Ĺterge',
    dateFormat: 'dd.mm.yyyy',
    timeFormat: 'hh:ii',
    firstDay: 1
};
 })(jQuery);