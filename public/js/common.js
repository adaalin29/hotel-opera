$(window).scroll(function() {
	if($(window).scrollTop() > 0) {
		$(".scroll-up").css("display","block");
	} else {
		$(".scroll-up").css("display","none");
	}
}); 

$(".scroll-up").click(function() {
	$("html, body").animate({ scrollTop: 0 }, "slow");
	return false;
  });

$(document).ready(function() {



	if(screen.width<=768){
		$('.galerie-swiper').addClass('container');
		$('.camere-swiper').addClass('container');
	}

	$(".back-button").click(function() {
		if($('.sidenav').hasClass('afisat')) {
			$('.sidenav').removeClass('afisat');

			$(".sidenav").css( {
					right:'100%'
				}

			);
		}
	}

);


	$(".topmenu").click(function() {
		if($('.sidenav').hasClass('afisat')) {
			$('.sidenav').removeClass('afisat');

			$(".sidenav").css( {
					right: 0+'px'
				}
			);
		}

		else {
			$('.sidenav').addClass('afisat');

			$(".sidenav").css( {
					right:'0'
				}

			);
		}
	}

);

	$(".multilanguage-text").click(function() {
		if($('.multilanguage-container').hasClass('afisat')) {
			$('.multilanguage-container').removeClass('afisat');

			$(".multilanguage-container").css( {
					height: 0+'px'
				}
			);
		}

		else {
			$('.multilanguage-container').addClass('afisat');

			$(".multilanguage-container").css( {
					height:'60'
				}

			);
		}
	}

);


	$(".rezerva-button").click(function() {
		if($('.rezervation').hasClass('afisat')) {
			$('.rezervation').removeClass('afisat');

			$(".rezervation").css( {
					left: 0+'px'
				}
			);

			$(".reservation-overlay").css( {
					left: 0+'px'
				}
			);
		}

		else {
			$('.rezervation').addClass('afisat');

			$(".reservation-overlay").css( {
				left: 0+'px'
			}
				);

			$(".rezervation").css( {
					left:'0'
				}

			);
		}
	}

);

	AOS.init();

	var swiper = new Swiper('.facilitati-camere-mobile .swiper-container', {
		navigation: {
		  nextEl: '.swiper-button-next',
		  prevEl: '.swiper-button-prev',
		},
	  });

	$('.pop-up-button').css('right','5%');
	// setTimeout(function() {   //calls click event after a certain time
	// 	$('.pop-up-button').css('transform','rotate(20deg)');
	// 	$('.pop-up-button').css('transform','rotate(0deg)');
	// 	$('.pop-up-button').css('transform','rotate(-20deg)');
	// 	$('.pop-up-button').css('transform','rotate(0deg)');
	//  }, 5000);





	  $('.reservation-button-header').click(function() {
		if($('.rezervation').hasClass('afisat')) {
			$('.rezervation').removeClass('afisat');

			$(".rezervation").css( {
					height: 0+'px'
				}
			);

		}

		else {
			$('.rezervation').addClass('afisat');


			$(".rezervation").css( {
					height:'80px'
				}

			);
		}
	}
);






	  if (!Cookies.get('pop-up-mic')) {
		//   $('.pop-up-mic').addClass('is-active');
		//   $('.pop-up-mic').css('left','0%');
		setTimeout( function(){ 
			$('.pop-up-mic').css('left','0%');
			$('.pop-up-mic').addClass('is-active');
		},2000); 

		}
	
	  $('.pop-up-mic-close').on('click', function () {
		  $('.pop-up-mic').removeClass('is-active');
		  Cookies.set('pop-up-mic', Date.now(), { expires: 365 });
	  });



	$( ".pop-up-button" ).click(function() {
		$('.pop-up-mic').css('left','0');
		if(screen.width<=1366){
			$('.pop-up-mic').css('left','0');
		}
		if(screen.width<=1024){
			$('.pop-up-mic').css('left','0');
		}
		if(screen.width<=768){
			$('.pop-up-mic').css('left','0');
		}
		if(screen.width<=480){
			$('.pop-up-mic').css('left','0px');
		}
	  });

	  $('.close-pop-up').click(function(){
		$('.pop-up-overlay').css('right','100%');
	  });

	  $('.close-pop-up-reservation').click(function(){
		  $(".reservation-overlay").css( {
			left: 100+'%'
		}
			);
		$('.rezervation').css('left','100%');
	  });


	  $('.pop-up-mic-close').click(function(){
		$('.pop-up-mic').css('left','100%');
	  });
	  


	// $(window).scroll(function() {
	// 	if ($(this).scrollTop() < 100 && screen.width>=1025) { // this refers to window
	// 		$(".rezervation").css('background-color','rgba(255,255,255,0.5)');
			
	// 	}

	// 	if ($(this).scrollTop() > 200 && screen.width>=1025) { // this refers to window
	// 		$(".rezervation").css('height','0');
	// 		console.log('yes');
	// 	}
	// 	if ($(this).scrollTop() < 200 && screen.width>=1025) { // this refers to window
	// 		$(".rezervation").css('height','132px');
	// 		console.log('yes');
	// 	}
	// 	if ($(this).scrollTop() >= 100 && screen.width>=1025) { // this refers to window
	// 		$(".rezervation").css('background-color','white');
	// 	}
	// });
	if(screen.width<=1024)
	$(".rezervation").prependTo(".reservation-overlay");

	$('.datepicker-here').datepicker({
		autoClose:true,
		language: 'en',
		minDate: new Date() // Now can select only dates, which goes after today
	})
})
